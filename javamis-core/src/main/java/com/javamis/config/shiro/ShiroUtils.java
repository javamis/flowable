/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.config.shiro;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * @name:   Shiro工具类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class ShiroUtils {
	/**
	 * @Description 获取当前登录用户的信息
	 *                                             从SecurityUtils里边创建一个 subject
	 * @return	返回当前登录用户的信息
	 */
	public static Subject getSubject() {
		return SecurityUtils.getSubject();
	}
	
	/**
	 * @Description 获取当前登录者对象
	 * @return  返回null表示没有登录，重定向到登录页面 ;
	 */
	public static Object getPrincipal() {
		Subject subject =  getSubject();
		if(subject != null) {
			return subject.getPrincipal();
		}
		return null;
	}
	
	/**
	 * @Description 获取当前登录者主要主体信息
	 * @return
	 */
	public static Object getPrimaryPrincipal() {
		Object principal = getPrincipal();
		if(principal != null) {
			return getSubject().getPrincipals().getPrimaryPrincipal();
		}
		return null;
	}
	
	/**
	 * @Description 获取ShiroInfo信息
	 * @return
	 */
	public static ShiroInfo getShiroInfoObj() {
		if(getPrimaryPrincipal() != null) {
			return (ShiroInfo)getPrimaryPrincipal();
		}
		return null;
	}
	
	/**
	 * @Description 返回当前登录用户的ShiroInfo对象信息;
	 * @return	true:已经过身份验证有权限; false:未经过身份验证无权限;
	 * @return
	 */
	public static ShiroInfo getShiroInfo() {
		return (ShiroInfo)getPrincipal();
	}
	
	/**
	 * @Description 当前请求是否已经过身份验证,是否有权限
	 * @return	true:已经过身份验证有权限; false:未经过身份验证无权限;
	 */
	public static boolean hasAuthority() {
		Subject subject = getSubject();
		if(subject !=null) {
			return subject.isAuthenticated();
		}
		return false;
	}
	
	/**
	 * @Description 验证当前登录用户是否有指定的权限; beetl模板引擎前端页面配置会调用使用该访问
	 * @param p
	 * @return
	 */
	public boolean hasPermission(String permission) {
        return getSubject() != null && getSubject().isPermitted(permission);
    }
	
	/**
	 * @Description 系统退出当前登录操作
	 * @return true:退出成功; false:退出失败;
	 */
	public static boolean logout() {
		Subject subject = getSubject();
		if(subject !=null) {
			subject.logout();
			return true;
		}
		return false;
	}
	
	/**
	 * 重定向URL地址
	 */
	public static void redirectUrl(HttpServletResponse response,String url) {
		try {
			response.sendRedirect(url);
			//return "redirect:"+"/";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
