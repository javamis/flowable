/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.config.shiro;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.shiro.authc.AccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javamis.modular.system.dao.DepartmentMapper;
import com.javamis.modular.system.dao.MenuMapper;
import com.javamis.modular.system.dao.RoleMapper;
import com.javamis.modular.system.dao.UserMapper;
import com.javamis.modular.system.entity.Menu;
import com.javamis.modular.system.entity.Role;
import com.javamis.modular.system.entity.User;

/**
 * @name:   Shiro工厂处理类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Service
@Transactional(readOnly = true)
//@DependsOn("appContextUtils")//依赖于AppContextUtils.java类
public class ShiroFactory implements IShiroFactory{
	private static Logger LOGGER = LoggerFactory.getLogger(ShiroFactory.class);
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private DepartmentMapper departmentMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private MenuMapper menuMapper;
	
//	public static IShiroFactory getContext() {
//        return AppContextUtils.getBean(IShiroFactory.class);
//    }

	@Override
	public User getUserByLoginCode(String userCode) {
		User user = userMapper.selectUserByLoginCode(userCode);
		if(user == null) {
			LOGGER.info("该用户名不存在");
			throw new AccountException("该用户名不存在");
		}
        if(user.getDeptId() == 0) {
        	LOGGER.info("-------该用户部门为空或0，导致无法获取到该用户的部门信息，请检查--------");
        	throw new AccountException("该用户部门为空!请检查");
        }
		return user;
	}

	@Override
	public String getDeptNameByDeptId(int deptId) {
		String deptName = departmentMapper.selectById(deptId).getFullName();
		return deptName;
	}

	@Override
	public Role getRoleById(String roleId) {
		Role role = roleMapper.selectById(roleId);
		return role;
	}

	@Override
	public Set<String> getStringPermissions(ShiroInfo shiroInfo) {
		Set<String> stringSet = new HashSet<>();
    	for(int roleId:shiroInfo.getRoleIdList()) {
    		List<Menu> menus = menuMapper.queryMenuUrlByRoleIds(roleId);
    		for(Menu menu:menus) {
    			String permission = menu.getPermission();
    			if(permission != null && permission !=""&&!permission.equals("")&& !stringSet.contains(permission)) {
    				stringSet.add(permission);	//循环添加权限,如:stringSet.add("user:show");
    			}
    			
    		}
    	}
		return stringSet;
	}

	@Override
	public ShiroInfo getShiroInfo(User user) {
		String[] roleArray = user.getRoleIds().split(",");
        List<Integer> roleIdList = new ArrayList<Integer>();
        List<String> roleNameList = new ArrayList<String>();
        String roleAbbreviates = "";
        for(String i:roleArray) { 
        	Role role = roleMapper.selectById(i);
        	roleIdList.add(Integer.valueOf(i)); 
        	roleNameList.add(role.getName());
        	roleAbbreviates = ","+role.getAbbreviate();
        }
        roleAbbreviates = roleAbbreviates.substring(1, roleAbbreviates.length());
        
        String deptName = departmentMapper.selectById(user.getDeptId()).getFullName();
		ShiroInfo shiroInfo = new ShiroInfo(user,deptName);
		shiroInfo.setRoleIds(user.getRoleIds());
		shiroInfo.setRoleIdList(roleIdList);
		shiroInfo.setRoleNames(roleNameList);
		shiroInfo.setRoleAbbreviates(roleAbbreviates);
		return shiroInfo;
	}
}
