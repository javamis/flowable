/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.config.shiro;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import com.javamis.common.util.EncryptionUtils;
import com.javamis.modular.system.entity.User;

import java.util.Set;

/**
 * @name:   Shiro自定义权限和身份认证类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class ShiroCustomRealm extends AuthorizingRealm {
	@Autowired
	private IShiroFactory shiroFactory;	
	
	/**
	 * @Description 权限认证方法。
	 *                                             使用场景:当html页面的<% if(shiro.hasPermission("user:all")){ %> 判断时会请求该方法!
	 */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
    	System.out.println("-------权限认证方法--------");
    	//String loginName = (String) SecurityUtils.getSubject().getPrincipal();
    	ShiroInfo shiroInfo = (ShiroInfo)principalCollection.getPrimaryPrincipal();
    	//IShiroFactory shiroFactory = ShiroFactory.getContext();
    	Set<String> stringSet = shiroFactory.getStringPermissions(shiroInfo);
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.setStringPermissions(stringSet);//设置用户权限
        return simpleAuthorizationInfo;
    }

    /**
     * @Description 使用场景:用户登录时访问该方法.
     * 				获取即将需要认证的信息
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("-------身份认证方法--------");
        String userCode = (String) authenticationToken.getPrincipal();
        if (userCode == null) {throw new AccountException("用户名为空，请输入可用的用户名！");} 
        //User user = userMapper.selectUserByLoginCode(userCode);
        //IShiroFactory shiroFactory = ShiroFactory.getContext();
        User user = shiroFactory.getUserByLoginCode(userCode);
        ShiroInfo shiroInfo = shiroFactory.getShiroInfo(user);
        //1.1前台用户输入的名文密码
        String inputPwd = new String((char[]) authenticationToken.getCredentials());
        //1.2名文通过MD5Pwd加密后的密码
        String userPwd = EncryptionUtils.MD5Pwd(userCode,inputPwd);
        //2根据用户名从数据库获取密码
        String password = user.getPassword();
        if (!userPwd.equals(password )) {throw new AccountException("密码错误，请输入正确的密码！");}
        //return new SimpleAuthenticationInfo(loginCode, password,ByteSource.Util.bytes(loginCode + "salt"), getName());
        return new SimpleAuthenticationInfo(shiroInfo, password,ByteSource.Util.bytes(userCode + "salt"), getName());
    }
}

