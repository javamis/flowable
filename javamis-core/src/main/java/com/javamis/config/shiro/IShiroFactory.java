/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.config.shiro;

import java.util.Set;

import com.javamis.modular.system.entity.Role;
import com.javamis.modular.system.entity.User;

/**
 * @name:   Shiro工厂接口类.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface IShiroFactory {
	
	/**
	 * 根据登录账号查询用户
	 * @param account
	 * @return
	 */
	User getUserByLoginCode(String account);
	
	/**
	 * 根据部门Id 查询部门名称
	 * @param deptId
	 * @return
	 */
	String getDeptNameByDeptId(int deptId);
	
	/**
	 * 根据角色Id 查询角色
	 * @param roleId
	 * @return
	 */
	Role getRoleById(String roleId);
	
	ShiroInfo getShiroInfo(User user);
	
	Set<String> getStringPermissions(ShiroInfo shiroInfo);
}
