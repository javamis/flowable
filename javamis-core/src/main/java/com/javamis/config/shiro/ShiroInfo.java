/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.config.shiro;
import java.util.List;

import com.javamis.modular.system.entity.User;

import lombok.Data;

/**
 * @name:   Shiro消息类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
public class ShiroInfo {
	private int id;
	/**
	 * 账户
	 */
	private String userCode;
	/**
	 * 账号名称
	 */
	private String userName;
	/**
	 * 部门-id
	 */
	private int departmentId;
	/**
	 * 部门-名称
	 */
	private String departmentName;
	/**
	 * 以逗号,分隔的角色字符串
	 */
	private String roleIds;
	/**
	 * 角色id集合
	 */
	private List<Integer> roleIdList;
	/**
	 * 角色名称集合
	 */
	private List<String> roleNames;
	/**
     *角色 英文简称
	*/
	private String roleAbbreviates;
	
	public ShiroInfo(User user,String deptName) {
		super();
		this.id = user.getId();
		this.userCode = user.getUserCode();
		this.userName = user.getUserName();
		this.departmentId = user.getDeptId();
		this.departmentName = deptName;
	}
}
