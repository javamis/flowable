/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.dao;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javamis.modular.system.entity.MenuTree;
import com.javamis.modular.system.entity.Role;
import com.javamis.modular.system.entity.RoleMenu;

/**
 * @name:   角色dao层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface RoleMapper extends BaseMapper<Role> {
	
	/**
	 * 查询部门信息
	 * @return
	 */
	@Select("SELECT m1.id AS id,(CASE WHEN (m2.id = 0 OR m2.id IS NULL) THEN 0 ELSE m2.id END ) AS parentId,m1.name AS name, ( CASE WHEN (m2.id = 0 OR m2.id IS NULL) THEN 'true' ELSE 'false' END ) as isOpen FROM sys_menu m1 LEFT JOIN sys_menu m2 ON m1.parentId = m2.id ORDER BY m1.id ASC")
	List<MenuTree> getMenuTree();
	
	/**
	 * 根据角色id和菜单id,插入角色菜单表
	 * @param roleMenu
	 * @return
	 */
	@Insert("insert into sys_role_menu(roleid,menuid) values(#{roleid},#{menuid})")
	public boolean insertRoleMenu(RoleMenu roleMenu);
	/**
	 * 根据角色id获取对应的所有菜单ids
	 * @param roleMenu
	 * @return
	 */
	@Select("select menuid from sys_role_menu where roleid = #{roleid}")
	List<RoleMenu> getMenusByRoleId(RoleMenu roleMenu);
	
	@Delete("delete from sys_role_menu where roleid = #{roleid} ")
	public boolean deleteRoleMenuByRoleId(RoleMenu roleMenu);
	
	/**
	 *查询所有角色，为用户分配角色树使用
	 * @param roleMenu
	 * @return
	 */
	@Select("select id as id,parentId as parentId,name as name from sys_role")
	public List<Role> getRoleTree();
	
	/**
	 * @Description 查询所有角色信息
	 * @param user
	 * @return
	 */
	@Select("SELECT * from sys_role")
	public List<Role> selectAllRoles();
	
	/**
	 * @Description 按角色名称查询角色列表
	 * @param name
	 * @return
	 */
	@Select("SELECT * from sys_role where NAME LIKE CONCAT('%',#{roleName},'%')")
	public List<Role> selectRolesByName(@Param("roleName") String roleName);
}
