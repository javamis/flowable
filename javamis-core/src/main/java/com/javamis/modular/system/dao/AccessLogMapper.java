/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javamis.common.logger.AccessLog;

/**
 * @name:   用户dao层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface AccessLogMapper extends BaseMapper<AccessLog> {

}
