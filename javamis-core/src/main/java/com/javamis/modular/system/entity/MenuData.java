/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;
import lombok.Data;

/**
 * @name:   菜单节点数据实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
public class MenuData {
	/**
	 * 子模块Id
	 */
	private String F_ModuleId;
	/**
	 * 父模块Id
	 */
	private String F_ParentId;
	/**
	 * 菜单名称
	 */
	private String F_FullName;
	/**
	 * 菜单图标
	 */
	private String F_Icon;
	/**
	 * 菜单链接地址
	 */
	private String F_UrlAddress;
	/**
	 * 是否是菜单(0:父菜单; 1:子菜单)
	 */
	private int F_SysMenu;

	@Override 
	public String toString() { 
		return "{"+
				"\"F_ModuleId\""+":" +"\""+F_ModuleId +"\""+ 
				", \"F_ParentId\""+":"+"\""+ F_ParentId+"\""+ 
				", \"F_FullName\""+":"+"\""+ F_FullName+"\""+ 
				", \"F_Icon\""+":"+"\""+ F_Icon+"\""+ 
				", \"F_UrlAddress\""+":"+"\""+F_UrlAddress+"\""+ 
				", \"F_SysMenu\""+":" + F_SysMenu +
				'}';
	}
}
