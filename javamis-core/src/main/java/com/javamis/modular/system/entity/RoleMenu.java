/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;
import lombok.Data;

/**
 * @name:   角色菜单实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
public class RoleMenu {
	/**
	 * 角色Id
	 */
	private int roleid;
	/**
	 * 菜单Id
	 */
	private int menuid;
	
	
	public RoleMenu(int roleid) {
		super();
		this.roleid = roleid;
	}

	public RoleMenu(int roleid, int menuid) {
		super();
		this.roleid = roleid;
		this.menuid = menuid;
	}

	public RoleMenu() {
		super();
	}
}
