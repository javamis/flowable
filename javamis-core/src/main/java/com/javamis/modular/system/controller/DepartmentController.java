/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javamis.common.message.Message;
import com.javamis.modular.system.dao.DepartmentMapper;
import com.javamis.modular.system.entity.Department;

import javax.validation.Valid;

/**
 * @name:   部门Controller层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Controller
@RequestMapping("/department")
@SuppressWarnings("rawtypes")	//抑制单类型警告
public class DepartmentController{
	
	@Autowired
	private DepartmentMapper departmentMapper;
    
    
    /**
               * 部门查询列表
     */
    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String list(Model model) {
    	return "system/department/list";
    }
    
    
    
    
    /**
     * 获取所有部门列表
     */
    @RequestMapping(value = "/listData")
    @ResponseBody
    public Object list(Department department) {
    	List<Department> deptlist = new ArrayList<Department>();
    	if(department.getIsTreeClick()==0) {
    		return departmentMapper.getDepartmentsByCondition(department.getCode(),department.getBriefName(),department.getFullName(),department.getType(),department.getStatus());
    	}else {
    		deptlist = departmentMapper.getDepartments(department.getId());
    	}
    	return deptlist;
    }
    
    /**
               * 部门表单页面
     * @param model
     * @return
     */
    @RequestMapping(value = "form")
    public String form() {
    	return "system/department/form";
    }
    
    /**
     * 添加部门
     */
    @RequestMapping("/save")
    @ResponseBody
    public Message save(@Valid Department department) {
    	if (department.getParentId()==null) {
    		department.setParentIds("0,");
        } else {
        	String parentIds = departmentMapper.selectById(department.getParentId()).getParentIds();
            department.setParentIds(parentIds  + department.getParentId() + ",");
        }
    	department.setUpdateTime(new Date());
        this.departmentMapper.insert(department);
        return Message.success();
    }
    
    /**
                * 根据部门id，修改部门信息
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
    	Department department = departmentMapper.selectById(id);
    	model.addAttribute(department);
    	return "system/department/edit";
    }
    
    /**
     * 修改部门
     */
    @RequestMapping("/edit")
    @ResponseBody
    public Message edit(@Valid Department department, BindingResult result) {
    	department.setUpdateTime(new Date());
        this.departmentMapper.updateById(department);
        return Message.success();
    }

    /**
     * 删除部门
     */
    @RequestMapping(value="delete")
    @ResponseBody
    public Message delete(@RequestParam Integer id) {
    	List<Department> depts = departmentMapper.getDepartmentsById(id);
    	for(Department dept :depts) {
    		this.departmentMapper.deleteById(dept.getId());
    	}
        return Message.success();
    }
    
    /**
     * @description 获取全部部门树，不传查询参数的方法
     * @param deptId
     * @return
     */
    @ResponseBody
	@RequestMapping("/getDeptTree")
	public List<Department> getDeptTree() {
		return departmentMapper.getDeptTree();
	}
    
    /**
     * @description 获取部分部门树
     * @param deptId
     * @return
     */
    @ResponseBody
	@RequestMapping("/getDeptTree/{deptId}")
	public List<Department> getDeptTree(@PathVariable Integer deptId) {
		if(deptId == 0 ) {
			return departmentMapper.getDeptTree();
		}else {
			return departmentMapper.getDeptTreeByParentId(deptId);
		}
	}
    
    /**
     * @description 获取部分部门树,按前台传来的部门id查询部门的方法
	*/
	@RequestMapping(value = "tree/{deptId}/{curentIframeId}",method = RequestMethod.GET)
	public String tree(@PathVariable String deptId, @PathVariable String curentIframeId,Model model) {
		System.out.println(curentIframeId);
		if(curentIframeId !="undefined"&&!curentIframeId.equals("undefined") ) {
			model.addAttribute("deptId",deptId);
			model.addAttribute("curentIframeId",curentIframeId);
		}
		return "system/department/departmentTree";
	}
	
	/**
     * @description 获取部分部门树,按前台传来的部门id查询部门的方法
	*/
	@RequestMapping(value = "tree/{specifyDeptId}/{specifyDeptName}/{deptId}/{curentIframeId}",method = RequestMethod.GET)
	public String tree(@PathVariable String specifyDeptId,@PathVariable String specifyDeptName,@PathVariable String deptId, @PathVariable String curentIframeId,Model model) {
		System.out.println(curentIframeId);
		if(curentIframeId !="undefined"&&!curentIframeId.equals("undefined") ) {
			model.addAttribute("specifyDeptId",specifyDeptId);
			model.addAttribute("specifyDeptName",specifyDeptName);
			model.addAttribute("deptId",deptId);
			model.addAttribute("curentIframeId",curentIframeId);
		}
		return "system/department/departmentTree";
	}
}
