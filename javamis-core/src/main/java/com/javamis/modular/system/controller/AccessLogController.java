/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.controller;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javamis.common.logger.AccessLog;
import com.javamis.common.message.Message;
import com.javamis.common.page.PageWarpper;
import com.javamis.modular.system.dao.AccessLogMapper;
import com.javamis.modular.system.dao.DictDataMapper;


/**
 * @name:   访问日志Controller层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Controller
@RequestMapping("/accessLog")
@SuppressWarnings("rawtypes")	//抑制单类型警告
public class AccessLogController {
	
	@Autowired
	private AccessLogMapper accessLogMapper;
		
	@Autowired
	DictDataMapper dictDataMapper;
	
	@Value("${ims.initPwd}")//从yml配置文件中获取isPreview值是true还是false.
    private String initPwd="123456";
    
    /**
     * 访问日志查询列表
     */
    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String list(Model model) {
    	return "system/logger/list";
    }
    
    @RequestMapping(value = "listData")
    @ResponseBody
    @SuppressWarnings("unlikely-arg-type")
    public PageWarpper<AccessLog> listData(AccessLog accessLog){
    	PageWarpper<AccessLog> pageWarpper = new PageWarpper<AccessLog>();
    	IPage<AccessLog> page = new Page<>(pageWarpper.getCurrentPage(), pageWarpper.getPageSize());//得到总页数
    	QueryWrapper<AccessLog> queryWrapper = new QueryWrapper<>();
    	String sort = pageWarpper.getSortName();
		String sortOrder =  pageWarpper.getSortOrder();
    	Map<String, Object> queryMap = accessLog.setData(accessLog);
		if(queryMap.isEmpty() && sort == null) {
			accessLogMapper.selectPage(page, null);
		}if(queryMap.isEmpty() && sort != null) {
    		 if(sortOrder.equals("desc")) {
    			 queryWrapper.orderByDesc(sort);
    		 }if(sortOrder.equals("asc")) {
    			 queryWrapper.orderByAsc(sort);
    		 }
    		 accessLogMapper.selectPage(page, queryWrapper);
    	}if(!queryMap.isEmpty()) {
    		 queryWrapper.allEq(queryMap);
    		 accessLogMapper.selectPage(page, queryWrapper);
    	}
    	pageWarpper.setTotal(new Long(page.getTotal()).intValue());
    	List<AccessLog> accessLogs = page.getRecords();
    	pageWarpper.setRows(accessLogs);
    	return  pageWarpper;
    }
    
    
    /**
     * 访问日志表单页面
     * @param model
     * @return
     */
    @RequestMapping(value = "form")
    public String form() {
    	return "system/logger/form";
    }
    
    /**
     * 添加访问日志
     */
    @RequestMapping("/save")
    @ResponseBody
    public Message<String> save(@Valid AccessLog accessLog) {
        this.accessLogMapper.insert(accessLog);
        return Message.success("添加访问日志成功啦!");
    }
    
    /**
     * 根据访问日志id，修改访问日志信息
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
    	AccessLog accessLog = accessLogMapper.selectById(id);
    	model.addAttribute(accessLog);
    	return "system/logger/edit";
    }
    
    /**
     * 修改访问日志
     */
    @RequestMapping("/edit")
    @ResponseBody
    public Message<String> edit(@Valid AccessLog accessLog, BindingResult result) {
        this.accessLogMapper.updateById(accessLog);
        return Message.success("修改访问日志成功啦!");
    }

    /**
     * 删除访问日志
     */
    @RequestMapping(value="delete")
    @ResponseBody
    public Message delete(@RequestParam Integer id) {
        this.accessLogMapper.deleteById(id);
        return Message.success();
    }
}
