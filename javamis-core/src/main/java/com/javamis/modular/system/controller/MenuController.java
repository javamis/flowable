/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javamis.common.message.Message;
import com.javamis.modular.system.dao.MenuMapper;
import com.javamis.modular.system.entity.Menu;

import javax.validation.Valid;

/**
 * @name:   菜单Controller层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Controller
@RequestMapping("/menu")
@SuppressWarnings("rawtypes")	//抑制单类型警告
public class MenuController{
	
	@Autowired
	private MenuMapper menuMapper;
    
    
    /**
     * 用户查询列表
     */
    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String list(Model model) {
    	return "system/menu/list";
    }
    
    /**
     * 查询树结构
	*/
	@RequestMapping(value = "tree",method = RequestMethod.GET)
	public String tree(Model model) {
	return "system/menu/menuTree";
	}
    
    
    /**
     * 获取所有菜单列表
     */
    @RequestMapping(value = "/listData")
    @ResponseBody
    public Object list(Menu menu) {
    	List<Menu> deptlist = new ArrayList<Menu>();
    	deptlist = menuMapper.getMenus(menu.getName(),menu.getHref(),menu.getSysMenu(),menu.getStatus());
    	return deptlist;
    }
    
    /**
     * 用户表单页面
     * @param model
     * @return
     */
    @RequestMapping(value = "form")
    public String form() {
    	return "system/menu/form";
    }
    
    /**
     * 添加用户
     */
    @RequestMapping("/save")
    @ResponseBody
    public Message save(@Valid Menu menu) {
    	if (menu.getParentId()==null) {
    		menu.setParentId(0);
    		menu.setParentIds("0,");
        } else {
        	String parentIds = menuMapper.selectById(menu.getParentId()).getParentIds();
        	menu.setParentIds(parentIds  + menu.getParentId() + ",");
        }
        this.menuMapper.insert(menu);
        return Message.success();
    }
    
    /**
     * 根据用户id，修改用户信息
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
    	Menu menu = menuMapper.selectById(id);
    	model.addAttribute(menu);
    	return "system/menu/edit";
    }
    
    /**
     * 修改用户
     */
    @RequestMapping("/edit")
    @ResponseBody
    public Message edit(@Valid Menu menu, BindingResult result) {
//    	menu.setUpdateTime(new Date());
        this.menuMapper.updateById(menu);
        return Message.success();
    }

    /**
     * 删除用户
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    public Message delete(@RequestParam Integer id) {
    	//List<Menu> depts = menuMapper.getMenusById(id);
//    	for(Menu dept :depts) {
//    		this.menuMapper.deleteById(dept.getId());
//    	}
    	menuMapper.deleteMenusById(id+"");
        return Message.success();
    }
    
    /**
     * 查询树结构
	*/
	@RequestMapping(value = "tree/{curentIframeId}",method = RequestMethod.GET)
	public String tree(@PathVariable String curentIframeId,Model model) {
		System.out.println(curentIframeId);
		if(curentIframeId !="undefined"&&!curentIframeId.equals("undefined") ) {
			model.addAttribute(curentIframeId);
		}
	return "system/menu/menuTree";
	}
	
	@ResponseBody
	@RequestMapping("/getTree")
	public List<Menu> getTree() {
		List<Menu> menus = menuMapper.getTree();
		return menus;
	}

}
