/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @name:   部门实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_department")
public class Department implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/*
	 * 部门主键id
	 */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	/**
	 * 父部门id
	 */
	@TableField(value = "parentId")
	private Integer parentId;
	/**
	 * 父部门名称
	 */
	@TableField(value = "parentName")
	private String parentName;
	/**
	 * 父级ids
     */
	@TableField(value = "parentIds")
	private String parentIds;
	/**
	 * 部门级别 
	 */
	private Integer level;
	/**
	 *是否有子节点: 默认为0  当为1的时候表示有多个节点,并且该节点下还有子节点 
	 */
	@TableField(value = "hasNode")
	private Integer hasNode;
	/**
	 * 部门编码
	 */
	private String code;
	/**
	 * 部门-缩略名称
	 */
	@TableField(value = "briefName")
	private String briefName;
	/**
	 * 部门-全名称
	 */
	@TableField(value = "fullName")
	private String fullName;
	/**
	 * (院/集团)分管领导编号
	 */
	@TableField(value = "fgLeaderCode")
	private String fgLeaderCode;
	/**
	 * (院/集团)分管领导姓名
	 */
	@TableField(value = "fgLeaderName")
	private String fgLeaderName;
	/**
	 * 部门负责人(部门主管)编号
	 */
	@TableField(value = "leaderCode")
	private String leaderCode;
	/**
	 * 部门负责人(部门主管)姓名
	 */
	@TableField(value = "leaderName")
	private String leaderName;
	/**
	 * 部门主任编号
	 */
	@TableField(value = "directorCode")
	private String directorCode;
	/**
	 * 部门主任姓名
	 */
	@TableField(value = "directorName")
	private String directorName;
	/**
	 * 部门副主任编号
	 */
	@TableField(value = "deputyDirectorCode")
	private String deputyDirectorCode;
	/**
	 * 部门副主任姓名
	 */
	@TableField(value = "deputyDirectorName")
	private String deputyDirectorName;
	/**
     * 办公电话
     */
	private String phone;
	/**
	 * 联系地址
	 */
	private String address;
	/**
	 * 邮政编码
	 */
	@TableField(value = "postCode")
	private String postCode;
	/**
	 * 电子邮箱
	 */
	private String email;
	/**
	 * 部门类型
	 */
	private String type;
	/**
	 * 排序号
	 */
	@TableField(value = "orderNumber")
	private Integer orderNumber;
	/**
	 * 更新时间
	 */
	@TableField(value = "updateTime")
	private Date updateTime;
	/**
	 * 创建时间
	 */
	@TableField(value = "createTime")
	private Date createTime;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 状态(启用;  冻结;  删除;）
     */
	private String status;
	/**
	 * 是否部门树点击事件(0：不是;1:是)
	 */
	@TableField(exist = false)
	private int isTreeClick;
}
