/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @name:   字典类型实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_dict_type")
public class DictType {
	/**
	 * 主键id
	 */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	/**
	 * 字典名称
	 */
	private String name;
	/**
	 * 字典类型
	 */
	private String type;
	/**
	 * 创建/更新时间
	 */
	private Date time;
	/**
	 * 备注信息
	 */
	private String remark;
	/**
	 * 状态
	 */
	private String status;
	
	/**
	 * @description 设置检索条件数据
	 * @param dict
	 * @return
	 */
	public Map<String, Object> setData(DictType dict){
		Map<String, Object> queryMap = new HashMap<>();
		if(dict.getName() !="" && dict.getName() !=null) {
	 		queryMap.put("name", dict.getName());
	 	}if(dict.getType() !=""&& dict.getType() !=null) {
	 		queryMap.put("type", dict.getType());
	 	}if(dict.getStatus() !=""&& dict.getStatus() !=null) {
	 		queryMap.put("status", dict.getStatus());
	 	}if(dict.getRemark() !="" &&dict.getRemark() !=null) {
	 		queryMap.put("remark", dict.getRemark());
	 	}
	 	return queryMap;
	}
}
