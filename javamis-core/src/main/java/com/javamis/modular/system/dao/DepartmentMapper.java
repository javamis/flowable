/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javamis.modular.system.entity.Department;

/**
 * @name:   部门dao层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface DepartmentMapper extends BaseMapper<Department> {

	/**
	  *按部门id查询部门List
	  */
	public List<Department> getDepartments(@Param("deptId") int id);
	
	/**
	  *按部门id查询部门List
	  */
	public List<Department> getDepartmentsById(@Param("deptId") int id);
	
	/**
	  *按条件查询部门List
	  */
	public List<Department> getDepartmentsByCondition(@Param("code") String code,@Param("briefName") String briefName,@Param("fullName") String fullName,@Param("type") String type,@Param("status") String status);
	
	/**
	  * 查询部门
	  */
	public List<Department> getDepartmentById(@Param("id") int id);
	
	/**
	 * 查询部门信息
	 * @return
	 */
	@Select("select id as id, parentId as parentId, briefName as briefName from sys_department")
	List<Department> getDeptTree();
	
	/**
	 * 根据父节点id,查询部门信息
	 * @return
	 */
	@Select("select id as id, parentId as parentId, briefName as briefName from sys_department WHERE id = #{deptId} or parentId = #{deptId}")
	List<Department> getDeptTreeByParentId(@Param("deptId") int deptId);
	/**
	 * 查询部门信息
	 * @return
	 */
	@Select("SELECT * FROM sys_department WHERE briefName = #{briefName}")
	List<Department> getDepartmentByName(@Param("briefName") String briefName);
	
	/**
	 * 查询部门所有信息
	 * @return
	 */
	@Select("SELECT * FROM sys_department")
	List<Department> getDepartmentList();
}
