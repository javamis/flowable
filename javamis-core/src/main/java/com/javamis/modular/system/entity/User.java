/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;


import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @name:   用户实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 头像
     */
	private String portrait;
    /**
     * 用户账号
     */
	@TableField(value = "userCode")
	private String userCode;
	/**
     * 用户姓名
     */
	@TableField(value = "userName")
	private String userName;
	/**
     * 密码
     */
	private String password;
    /**
     * md5加密密码
     */
	@TableField(value = "passwordMD5")
	private String passwordMD5;
	/**
     * 所属部门编码
     */
	@TableField(value = "deptId")
	private int deptId;
	/**
     * 所属部门名称
     */
	@TableField(value = "deptName")
	private String deptName;
	/**
	 * 归属机构父编码
	 */
	@TableField(exist = false)
	private int departmentPid;
	/**
     * 电子邮箱
     */
	private String email;
	/**
     * 办工电话
     */
	private String phone;
	/**
     * 手机号码
     */
	private String mobile;
	 /**
     * 创建时间
     */
	@TableField(value = "createTime")
	private Date createTime;
	 /**
     * 更新时间
     */
	@TableField(value = "updateTime")
	private Date updateTime;
    /**
     * 生日
     */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;
    /**
     * 性别（1：男 2：女）
     */
	private Integer sex;
	
	/**
     * 性别名称（1：男 2：女）
     */
	@TableField(exist = false)
	private String sexName;
    
    
    /**
     * 拥有角色编号
     */
	@TableField(value = "roleIds")
	private String roleIds;
    
    /**
     * 状态(1：启用  2：冻结  3：删除）
     */
	private Integer status;

	public User() {}
	public User(Integer id, String roleIds) {
		this.id = id;
		this.roleIds = roleIds;
	}
	
	/**
	 * 查询条件
	 * @param dictData
	 * @return
	 */
	public Map<String, Object> setData(User user){
		Map<String, Object> queryMap = new HashMap<>();
		if(user.getUserCode() !="" && user.getUserCode() !=null) {
	 		queryMap.put("userCode", user.getUserCode());
	 	}if(user.getUserName() !=""&& user.getUserName() !=null) {
	 		queryMap.put("userName", user.getUserName());
	 	}if(user.getEmail() !=""&& user.getEmail() !=null) {
	 		queryMap.put("email", user.getEmail());
	 	}if(user.getPhone() !=""&& user.getPhone() !=null) {
	 		queryMap.put("phone", user.getPhone());
	 	}if(user.getSex() !=null&&user.getSex() !=0) {
	 		queryMap.put("sex", user.getSex());
	 	}if(user.getDeptId() !=0) {
	 		queryMap.put("deptId", user.getDeptId());
	 	}if(user.getDeptName() !=""&&user.getDeptName() !=null) {
	 		queryMap.put("deptName", user.getDeptName());
	 	}
	 	return queryMap;
	}
}
