/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @name:   字典数据实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_dict_data")
public class DictData {
	private int id;
	/**
	 * 字典类型-与sys_dict_type表type对应
	 */
	private String type;
	/**
	 *字典标签
	 */
	@TableField(value = "dictLabel")
	private String dictLabel;
	/**
	 *  字典值
	 */	
	@TableField(value = "dictValue")
	private String dictValue;
	/**
	 * 字典编码
	 */
	private String code;
	/**
	 * 创建字典的时间
	 */
	@TableField(value = "createTime")
	private Date createTime;
	/**
	 * 备注信息
	 */
	private String remark;
	/**
	 * 状态
	 */
	private String status;
	public DictData() {
		super();
	}
	
	/**
	 * 查询条件
	 * @param dictData
	 * @return
	 */
	public Map<String, Object> setData(DictData dictData){
		Map<String, Object> queryMap = new HashMap<>();
    	if(dictData.getType() !="" && dictData.getType() !=null) {
    		String type = dictData.getType().trim();
    		String[] typeArray;
    		if(type.contains(",")) {
    			typeArray = type.split(",");
    			queryMap.put("type", typeArray[0]);
    		}else {
    			queryMap.put("type", type);
    		}
 	 		
 	 	}if(dictData.getDictLabel() !="" && dictData.getDictLabel() !=null) {
	 		queryMap.put("dictLabel", dictData.getDictLabel().trim());
	 	}if(dictData.getDictValue() !=""&& dictData.getDictValue() !=null) {
	 		queryMap.put("dictValue", dictData.getDictValue().trim());
	 	}if(dictData.getCode() !=""&& dictData.getCode() !=null) {
	 		queryMap.put("code", dictData.getCode().trim());
	 	}if(dictData.getRemark() !=""&& dictData.getRemark() !=null) {
	 		queryMap.put("remark", dictData.getRemark().trim());
	 	}if(dictData.getStatus() !=""&& dictData.getStatus() !=null) {
	 		queryMap.put("status", dictData.getStatus());
	 	}
	 	return queryMap;
	}
}
