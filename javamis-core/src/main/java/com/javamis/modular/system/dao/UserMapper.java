/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.dao;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javamis.modular.system.entity.User;

/**
 * @name:   用户dao层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface UserMapper extends BaseMapper<User> {
	
	/**
	 * @description 根据用户id，更新用户表roleId的值
	 * @param roleIds
	 * @param userId
	 * @return
	 */
	@Update("update sys_user set roleIds = #{roleIds} where id =#{id}")
	public int updateRoleIdByUserId(User user);
	
	/**
	 * @Description 根据账号查询用户信息
	 * @param user
	 * @return
	 */
	@Select("SELECT * from sys_user where userCode = #{userCode}")
	public com.javamis.modular.system.entity.User selectUserByLoginCode(String userCode);
	
	/**
	 * @Description 根据多个账号查询用户List信息
	 * @param userCodes
	 * @return
	 */
	public List<com.javamis.modular.system.entity.User> selectUserListByLoginCodes(List<String> userCodes);
	
	/**
	 * @Description 根据账号查询用户信息
	 * @param user
	 * @return
	 */
	//@Select("SELECT * from sys_user where userCode = #{userCode}")
	@Select("SELECT * from sys_user where userCode LIKE CONCAT('%',#{userCode},'%')  or userName LIKE CONCAT('%',#{userCode},'%')")
	public List<User> selectUsersByUserCode(@Param("userCode") String userCode);
	
	/**
	 * @Description 查询所有用户信息
	 * @param user
	 * @return
	 */
	@Select("SELECT * from sys_user")
	public List<User> selectAllUsers();
}
