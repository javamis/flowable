/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @name:   菜单Tree树实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MenuTree {
	/**
	 * 节点id
	 */
	private Integer id;	
	/**
	 * 父节点id
	 */
	private Integer parentId;
	/**
	 * 节点名称
	 */
	private String name;
	/**
	 * 手机号
	 */
	private String phone;
	/**
	 * 是否打开节点
	 */
	private Boolean open;
	/**
	 * 是否被选中
	 */
	private Boolean checked;

}
