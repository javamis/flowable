/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.dao;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javamis.modular.system.entity.DictType;
import com.javamis.modular.system.entity.MenuTree;
import com.javamis.modular.system.entity.RoleMenu;

/**
 * @name:   字典dao层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface DictTypeMapper extends BaseMapper<DictType> {
	
	/**
	 * 查询部门信息
	 * @return
	 */
	@Select("SELECT m1.id AS id,(CASE WHEN (m2.id = 0 OR m2.id IS NULL) THEN 0 ELSE m2.id END ) AS parentId,m1.name AS name, ( CASE WHEN (m2.id = 0 OR m2.id IS NULL) THEN 'true' ELSE 'false' END ) as isOpen FROM sys_menu m1 LEFT JOIN sys_menu m2 ON m1.pcode = m2. CODE ORDER BY m1.id ASC")
	List<MenuTree> getMenuTree();
	
	/**
	 * 根据角色id和菜单id,插入角色菜单表
	 * @param roleMenu
	 * @return
	 */
	@Insert("insert into sys_role_menu(roleid,menuid) values(#{roleid},#{menuid})")
	public boolean insertRoleMenu(RoleMenu roleMenu);
	/**
	 * 根据角色id获取对应的所有菜单ids
	 * @param roleMenu
	 * @return
	 */
	@Select("select menuid from sys_role_menu where roleid = #{roleid}")
	List<RoleMenu> getMenusByRoleId(RoleMenu roleMenu);
	
	@Delete("delete from sys_role_menu where roleid = #{roleid} ")
	public boolean deleteRoleMenuByRoleId(RoleMenu roleMenu);
	
	/**
	 * 根据字典类型，获取对应的所有字典数据
	 * @param roleMenu
	 * @return
	 */
	@Select("select * from sys_dict_type where id = #{id}")
	DictType getDictTypeById(Integer id);
	
}
