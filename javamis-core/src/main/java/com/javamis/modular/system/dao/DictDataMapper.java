/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.dao;
import java.util.List;
import org.apache.ibatis.annotations.Select;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javamis.modular.system.entity.DictData;

/**
 * @name:   字典dao层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface DictDataMapper extends BaseMapper<DictData> {
	
	/**
	 * 根据字典类型，获取对应的所有字典数据
	 * @param type  字典类型
	 * @return
	 */
	@Select("select * from sys_dict_data where type = #{type}")
	public List<DictData> getDictDataByType(String type);
	
	/**
	  * 查询字典
	  */
	@Select("select * from sys_dict_data where type = #{type} order by code")
	public List<com.javamis.modular.system.entity.DictData> queryParam(String type);
}
