/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.dao;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javamis.modular.system.entity.Menu;
import com.javamis.modular.system.entity.MenuData;

/**
 * @name:   菜单dao层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface MenuMapper extends BaseMapper<Menu> {
	/**
	 * 按条件查询用户列表
	 * @param userCode
	 * @param userName
	 * @param email
	 * @param phone
	 * @param status
	 * @return
	 */
//	List<User> selectUserList(@Param("userCode") String userCode,@Param("userName") String userName,@Param("email") String email,@Param("phone") String phone,@Param("status") int status);
	/**
	  *按菜单id查询菜单List
	  */
	public List<Menu> getMenus(@Param("name") String name,@Param("href") String href,@Param("sysMenu") String sysMenu,@Param("status") Integer status);
	
	
	
	/**
	  *按条件查询菜单List
	  */
	public List<Menu> getMenusByCondition(@Param("code") String code,@Param("briefName") String briefName,@Param("fullName") String fullName,@Param("type") String type,@Param("status") String status);
	
	/**
	  * 查询菜单
	  */
	public List<Menu> getMenuById(@Param("id") int id);
	
	
	
	/**
	 * 根据角色获取菜单
     *
     * @param roleIds
     * @return
     * @date 2017年2月19日 下午10:35:40
     */
    List<MenuData> getMenusByRoles(List<Integer> roleIds);
    
    /**
	  * 根据roleId查询sys_menu表URL字段.
	  */
	@Select("SELECT permission from sys_menu where id in( SELECT menuid from sys_role_menu where roleid =#{roleId})")
	public List<Menu> queryMenuUrlByRoleIds(int roleId);
	
	/**
	 * 查询菜单信息
	 * @return
	 */
	@Select("select id as id, parentId as parentId, name as name from sys_menu")
	List<Menu> getTree();
	
	/**
	  *按菜单id,查询该该菜单及所有子菜单
	  */
	@Select("select * from sys_menu where FIND_IN_SET(#{menuId},parentIds) or id = #{menuId} ORDER BY code ASC")
	public List<Menu> getMenusById(@Param("menuId") int menuId);
	
	/**
	 * 根据菜单id,删除该菜单及所有子菜单
	 * @param roleMenu
	 * @return
	 */
	//MYSQL不识别这样写法所以报错，改成了下面的写法；ORACLE和SQLServer识别：DELETE from sys_menu_delete where parentIds in ( SELECT parentIds from sys_menu_delete a WHERE FIND_IN_SET('3',parentIds))
	@Delete("DELETE from sys_menu where parentIds in ( select a.parentIds from(SELECT parentIds from sys_menu a WHERE FIND_IN_SET(#{menuId},parentIds)) a) or id = #{menuId}")
	public boolean deleteMenusById(@Param("menuId") String menuId);
}
