/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @name:   角色实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_role")
public class Role{
	/**
               * 主键id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	/**
	     * 角色名称
	*/
	private String name;
	/**
	 * 父角色节点id
	 */
	@TableField(value = "parentId")
	private Integer parentId;
	/**
	 * 父角色节点名称
	 */
	@TableField(value = "parentName")
	private String parentName;
	/**
	     * 角色编码
	*/
	private Integer code;
    /**
               * 部门名称
     */
	@TableField(value = "deptId")
	private Integer deptId;
    /**
               *角色 英文简称
     */
	private String abbreviate;
    /**
              * 角色类型
     */
	@TableField(value = "roleType")
	private Integer roleType;
	
	/**
               * 角色类型名称（数据从字典role_type获取）
     */
	@TableField(exist = false)
	private String roleTypeName;
	/**
	 * 更新时间
	 */
	@TableField(value = "updateTime")
	private Date updateTime;
	/**
	 * 备注信息
	 */
	private String remark;
	/**
	 * 状态(正常; 停用;）
     */
	private String status;
	
	public Map<String, Object> setData(Role role){
		Map<String, Object> queryMap = new HashMap<>();
		if(role.getName() !="" && role.getName() !=null) {
	 		queryMap.put("name", role.getName());
	 	}if(role.getRoleType() !=null) {
	 		queryMap.put("roleType", role.getRoleType());
	 	}if(role.getCode() !=null) {
	 		queryMap.put("code", role.getCode());
	 	}if(role.getRemark() !="" &&role.getRemark() !=null) {
	 		queryMap.put("remark", role.getRemark());
	 	}if(role.getStatus() !=""&& role.getStatus() !=null) {
	 		queryMap.put("status", role.getStatus());
	 	}
	 	return queryMap;
	}
}
