/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @name:   菜单实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_menu")
public class Menu{

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
	 * 父id
	 */
	@TableField(value = "parentId")
	private Integer parentId;
	/**
	 * 父ids(为了删除时能够级联删除才增加了parentIds，parentIds除了删除没做其它使用!!)
	 */
	@TableField(value = "parentIds")
	private String parentIds;
	/**
	 * 父id名称
	 */
	@TableField(value = "parentName")
	private String parentName;
    
//    /**
//     * 菜单父编号
//     */
//    private String pcode;
//    /**
//     * 当前菜单的所有父菜单编号
//     */
//    private String pcodes;
    /**
     * 菜单名称
     */
    //@NotBlank
    private String name;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * url地址
     */
    //@NotBlank
    private String href;
    /**
     * 权限值
     */
    //@NotBlank
    private String permission;
    /**
     * 菜单编号
     */
//	@TableField(value = "orderNumber")
//	private Integer number;
    /**
     * 菜单排序号（菜单编号）
     */
    private Integer number;
    /**
     * 父菜单排序号(父菜单编号)
     */
    @TableField(value = "pNumber")
    private Integer pNumber;
//    /**
//     * 菜单层级
//     */
//    private Integer levels;
    /**
     * 是否是系统菜单（1：是  0：不是）
     */
    @TableField(value = "sysMenu")
    private String sysMenu;
//    /**
//     * 备注
//     */
//    private String tips;
    /**
     * 菜单状态 :  1:启用   0:不启用
     */
    private Integer status=1;
//    /**
//     * 是否打开:    1:打开   0:不打开
//     */
//    private Integer isopen;
    /**
     * 备注
     */
    private String remark;

}
