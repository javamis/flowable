/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.system.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javamis.common.message.Message;
import com.javamis.common.page.PageWarpper;
import com.javamis.modular.system.dao.DictDataMapper;
import com.javamis.modular.system.dao.DictTypeMapper;
import com.javamis.modular.system.entity.DictData;
import com.javamis.modular.system.entity.DictType;
import com.javamis.modular.system.entity.MenuTree;

/**
 * @name:   字典Controller层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Controller
@RequestMapping("/dict")
@SuppressWarnings("rawtypes")	//抑制单类型警告
public class DictController {
	
	@Autowired
	private DictTypeMapper dictTypeMapper;
	@Autowired
	private DictDataMapper dictDataMapper;
    
    /**
     * 角色查询列表
     */
    @RequestMapping(value = "listType",method = RequestMethod.GET)
    public String list(Model model) {
    	return "system/dict/list_type";
    }
    
    @RequestMapping(value = "listData")
    @ResponseBody
    public PageWarpper<DictType> listData(DictType dict){
    	PageWarpper<DictType> pageWarpper = new PageWarpper<DictType>();
    	IPage<DictType> page = new Page<>(pageWarpper.getCurrentPage(), pageWarpper.getPageSize());//得到总页数
    	QueryWrapper<DictType> queryWrapper = new QueryWrapper<>();
    	String sortOrder =  pageWarpper.getSortOrder();
    	Map<String, Object> queryMap = dict.setData(dict);
    	if(queryMap.isEmpty()) {
    		if(sortOrder.equals("desc")) {
    			queryWrapper.orderByDesc("id");
    		}if(sortOrder.equals("asc")) {
    			queryWrapper.orderByAsc("id");
    		}
    		dictTypeMapper.selectPage(page, queryWrapper);
    	}if(!queryMap.isEmpty()) {
    		 queryWrapper.allEq(queryMap);
    		 dictTypeMapper.selectPage(page, queryWrapper);
    	}
    	pageWarpper.setTotal(new Long(page.getTotal()).intValue());
    	List<DictType> users = page.getRecords();
    	pageWarpper.setRows(users);
    	return  pageWarpper;
    }
    
    
    /**
     * 字典类型表单页面
     * @param model
     * @return
     */
    @RequestMapping(value = "formType")
    public String formType() {
    	return "system/dict/form_type";
    }
    
    
    /**
     * 添加用户
     */
    @RequestMapping("/saveType")
    @ResponseBody
    public Message save(@Valid DictType dictType) {
    	dictType.setTime(new Date());
        this.dictTypeMapper.insert(dictType);
        return Message.success("恭喜您，保存新增用户: "+dictType.getName()+" 成功了");
    }
    
    /**
     * 根据用户id，修改用户信息
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="editType/{id}")
    public String editType(@PathVariable Integer id, Model model) {
    	DictType dict = dictTypeMapper.selectById(id);
    	model.addAttribute(dict);
    	return "system/dict/edit_type";
    }
    
    /**
     * 修改用户
     */
    @RequestMapping("/editType")
    @ResponseBody
    public Message editType(@Valid DictType dict, BindingResult result) {
    	dict.setTime(new Date());
        this.dictTypeMapper.updateById(dict);
        return Message.success();
    }
    
    /**
     * 删除用户
     */
    @RequestMapping(value="deleteDictType")
    @ResponseBody
    public Message delete(@RequestParam Integer id) {
        this.dictTypeMapper.deleteById(id);
        return Message.success();
    }
    
    /**
     * 添加用户
     */
    @RequestMapping("/saveData")
    @ResponseBody
    public Message save(@Valid DictData dictData) {
    	dictData.setCreateTime(new Date());
        this.dictDataMapper.insert(dictData);
        return Message.success();
    }
    
    /**
     * 根据用户id，修改用户信息
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="editData/{type}/{id}")
    public String editData(@PathVariable Integer id, Model model) {
    	DictData dictData = dictDataMapper.selectById(id);
    	model.addAttribute(dictData);
    	return "system/dict/edit_data";
    }
    
    /**
     * 修改用户
     */
    @RequestMapping("/editData")
    @ResponseBody
    public Message editData(@Valid DictData dict, BindingResult result) {
    	dict.setCreateTime(new Date());
        this.dictDataMapper.updateById(dict);
        return Message.success();
    }

    
    
    /**
     * 删除用户
     */
    
	@RequestMapping(value="deleteDictData")
    @ResponseBody
    public Message deleteDictData(@RequestParam Integer id) {
        this.dictDataMapper.deleteById(id);
        return Message.success();
    }
    
    /**
     * 根据字典类型，打开字典数据列表页面
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="listData/{type}")
    public String ListData(@PathVariable String type, Model model) {
//    	DictType dictType = dictTypeMapper.getDictTypeById(type);
    	model.addAttribute("type",type);
    	return "system/dict/list_data";
    }
    
    /**
     * 字典数据表单页面
     * @param model
     * @return
     */
    @RequestMapping(value = "formData/{type}")
    public String formData(@PathVariable String type,Model model) {
    	model.addAttribute("type",type);
    	return "system/dict/form_data";
    }
    
    /**
     * 用户表单页面
     * @param model
     * @return
     */
    @RequestMapping(value = "listDictData")
    @ResponseBody
    public PageWarpper<DictData> listDictData(DictData dictData){
    	PageWarpper<DictData> pageWarpper = new PageWarpper<DictData>();
        //得到总页数
    	IPage<DictData> page = new Page<>(pageWarpper.getCurrentPage(), pageWarpper.getPageSize());
    	QueryWrapper<DictData> queryWrapper = new QueryWrapper<>();
    	Map<String, Object> queryMap = dictData.setData(dictData);
    	String sortOrder =  pageWarpper.getSortOrder();
    	if(queryMap.isEmpty()) {
    		if(sortOrder.equals("desc")) {
    			queryWrapper.orderByDesc("id");
    		}if(sortOrder.equals("asc")) {
    			 queryWrapper.orderByAsc("id");
    		}
    		dictDataMapper.selectPage(page, queryWrapper);
    	}else{
    		queryWrapper.allEq(queryMap);
    		dictDataMapper.selectPage(page, queryWrapper);
    	}
    	pageWarpper.setTotal(new Long(page.getTotal()).intValue());
    	List<DictData> users = page.getRecords();
    	pageWarpper.setRows(users);
    	return  pageWarpper;
    }
    
    
    /**
     * 为角色授权菜单
     */
	/*
	 * @RequestMapping(value="authorityMenu")
	 * 
	 * @ResponseBody public Tip authorityMenu(@RequestParam("menuIds") String
	 * menuIds, @RequestParam("id") int id) { String[] dictIds = menuIds.split(",");
	 * DictMenu rm = new DictMenu(id); dictTypeMapper.deleteDictMenuByDictId(rm);
	 * for(int i=0;i<dictIds.length;i++) {
	 * rm.setMenuid(Integer.valueOf(dictIds[i])); dictTypeMapper.insertDictMenu(rm); }
	 * return ResultCode.SUCCESS; }
	 */
    
    
    @ResponseBody
	@RequestMapping("/getMenuTree")
	public List<MenuTree> getMenuTree() {
		List<MenuTree> dictTreeList = dictTypeMapper.getMenuTree();
		return dictTreeList;
	}
    
    /**
     * 查询数据字典
     */
    @RequestMapping(value = "/queryParam")
    @ResponseBody
    public List<DictData> queryParam(String type) {
    	DictData p = new DictData();
    	p.setType(type);
    	List<DictData> params = dictDataMapper.queryParam(type);
    	return params;
    }
    
    /**
     * 查询数据字典
     */
    @RequestMapping(value = "/queryDictData")
    @ResponseBody
    public List<DictData> queryDictData(String dictType) {
    	List<DictData> dictDatas = dictDataMapper.getDictDataByType(dictType);
    	return dictDatas;
    }
}
