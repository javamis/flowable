/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.logger;

/**
 * @name:   日志操作的平台模块单元枚举类.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public enum LoggerModule {
	/**
     * 被操作的 -未知模块单元
     */
    UNKNOWN("unknown"),
    /**
     * 被操作的 - 用户管理模块
     */
    USER("用户管理"),
    /**
     * 被操作的 - 部门管理模块
     */
    DEPT("部门管理"),
    /**
     * 被操作的 - 角色管理模块
     */
    ROLE("角色管理"),
    /**
     * 被操作的 - 菜单管理模块
     */
    MENU("菜单管理"),
    /**
     * 被操作的 - 字典管理模块
     */
    DICT("字典管理"),
    /**
     * 被操作的 - 系统管理模块
     */
    SYSTEM("系统管理"),
	/**
     * 被操作的 - 请假单-发流程示例模块
     */
	LEAVE("请假单-流程示例"),
	/**
     * 被操作的 - 资产管理模块
     */
	ASSSTORAGE("资产管理");
    
    //Redis("redis");
 
    private String value;
 
    LoggerModule(String value) {
        this.value = value;
    }
 
    public String getValue() {
        return value;
    }
 
    public void setValue(String value) {
        this.value = value;
    }

}
