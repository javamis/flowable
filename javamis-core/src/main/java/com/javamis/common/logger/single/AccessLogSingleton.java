/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.logger.single;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.javamis.common.logger.AccessLog;
import com.javamis.common.logger.factory.AccessLogFactory;
import com.javamis.modular.system.dao.AccessLogMapper;

/**
 * @name:   访问日志单例模式类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class AccessLogSingleton {
	private static  AccessLogSingleton accessLogSingleton = new AccessLogSingleton();
	//选择定时线程池的好处在于日志非实时要求那么高
	private ScheduledThreadPoolExecutor threadPool = new ScheduledThreadPoolExecutor(15);
	
	/**
	 * 让构造函数为 private，这样该类就不会被实例化
	 */
	private AccessLogSingleton() {}
	
	/**
	 * 	获取唯一可用的对象(优点：是线程安全的，效率高；缺点：容易产生垃圾对象)
	 */
	public static AccessLogSingleton getInstance() {
		return accessLogSingleton;
	}
	
	/**
	 * 运行日志方法，将日志存储至数据库
	 */
	public void runLog(AccessLogMapper accessLogMapper,AccessLog operationLog) {
		threadPool.schedule(AccessLogFactory.commonLog(accessLogMapper, operationLog), 9, TimeUnit.MILLISECONDS);//第一个参数表示将要执行的任务，9表示延迟执行的时间，TimeUnit.MILLISECONDS执行的时间间隔数值单位
	}

}
