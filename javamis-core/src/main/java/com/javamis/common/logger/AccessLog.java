/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.logger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;


/**
 * @name:   访问日志实体类.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_access_log")
public class AccessLog {
	/*
	 * 访问日志主键id
	 */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 操作类型
     */
    private String type;
    /**
     * 操作人账号
     */
    @TableField(value = "operCode")
    private String operCode;
    /**
	 * 操作人
	 */
	@TableField(value = "operUser")
	private String operUser;
	/**
     * 访问地址
     */
    private String address;
    /**
	 * 业务模块
	 */
	private String modular;
	/**
	 * 操作时间
	 */
	@TableField(value = "operTime")
	private Date operTime;
	/**
	 * 访问的IP地址
	 */
	@TableField(value = "ipAddress")
	private String ipAddress;
	/**
	 * 设备类型
	 */
	@TableField(value = "deviceType")
	private String deviceType;
	/**
	 * 浏览器类型
	 */
	@TableField(value = "browserType")
	private String browserType;
    /**
     * 日志等级
     */
    private Integer level;
    
    /**
     * 参数
     */
    private String args;
    /**
     * 日志描述
     */
    private String details;
    
    /**
     * 方法运行时间
     */
    @TableField(value = "runTime")
    private Integer runTime;
    /**
     * 方法返回值
     */
    @TableField(value = "returnValue")
    private String returnValue;
    
 
    @Override
    public String toString() {
        return "OperationLog{" +
                "id='" + id + '\'' +
                ", operTime=" + operTime +
                ", level=" + level +
                ", ipAddress=" + ipAddress +
                ", modular='" + modular + '\'' +
                ", address='" + address + '\'' +
                ", args='" + args + '\'' +
                ", operCode='" + operCode + '\'' +
                ", operUser='" + operUser + '\'' +
                ", details='" + details + '\'' +
                ", type='" + type + '\'' +
                ", runTime=" + runTime +
                ", returnValue='" + returnValue + '\'' +
                '}';
    }
    public Map<String, Object> setData(AccessLog accessLog){
		Map<String, Object> queryMap = new HashMap<>();
		if(accessLog.getType() !="" && accessLog.getType() !=null) {
	 		queryMap.put("type", accessLog.getType());
	 	}if(accessLog.getOperCode() !=""&& accessLog.getOperCode() !=null) {
	 		queryMap.put("operCode", accessLog.getOperCode());
	 	}if(accessLog.getModular() !=""&& accessLog.getModular() !=null) {
	 		queryMap.put("modular", accessLog.getModular());
	 	}if(accessLog.getIpAddress() !=""&& accessLog.getIpAddress() !=null) {
	 		queryMap.put("ipAddress", accessLog.getIpAddress());
	 	}
	 	return queryMap;
    }
    
}
