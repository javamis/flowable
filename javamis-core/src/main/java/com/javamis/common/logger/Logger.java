/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.logger;

import java.lang.annotation.*;

/**
 * @name:   自定义日志注解接口.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Logger {
	/**
     * 方法描述,可使用占位符获取参数:{{tel}}
     */
    String detail() default "";
 
    /**
     * 日志等级:自己定，此处分为1-9
     */
    int level() default 0;
 
    /**
     * 操作类型(enum):主要是select,insert,update,delete
     */
    LoggerType type() default LoggerType.UNKNOWN;
 
    /**
     * 被操作的对象(此处使用enum):可以是任何对象，如模块名(user)，或者是工具(redis)
     */
    LoggerModule unit() default LoggerModule.UNKNOWN;

}
