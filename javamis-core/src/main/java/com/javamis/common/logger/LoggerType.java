/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.logger;

/**
 * @name:   日志操作类型枚举类.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public enum LoggerType {
	/**
     * 未知类型
     */
    UNKNOWN("未知日志"),
    /**
     * 删除类型
     */
    DELETE("删除日志"),
    /**
     * 查询类型
     */
    SELECT("查询日志"),
    /**
     * 更新类型
     */
    UPDATE("更新日志"),
    /**
     * 添加类型
     */
    INSERT("新增日志");
 
	/**
	 * 日志操作值
	 */
    private String value;
 
    public String getValue() {
        return value;
    }
 
    public void setValue(String value) {
        this.value = value;
    }
 
    LoggerType(String s) {
        this.value = s;
    }
}
