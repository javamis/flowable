/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.logger.factory;

import java.util.TimerTask;

import com.javamis.common.logger.AccessLog;
import com.javamis.modular.system.dao.AccessLogMapper;

/**
 * @name:   访问日志工厂处理类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class AccessLogFactory {

	/**
	 * 普通公共业务日志线程
	 * @param accessLogMapper
	 * @param operationLog
	 * @return
	 */
	public static TimerTask commonLog(AccessLogMapper accessLogMapper,AccessLog operationLog) {
        return new TimerTask() {
            @Override
            public void run() {
                try {
                	accessLogMapper.insert(operationLog);
                } catch (Exception e) {
                    //logger.error("创建业务日志异常!", e);
                }
            }
        };
    }
}
