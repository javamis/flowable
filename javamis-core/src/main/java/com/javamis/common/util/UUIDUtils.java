/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.util;
import java.util.Date;

/**
 * @name:  Java中唯一数的生成.
 * 				唯一数的生成很简单，基本上以时间为基础进行生成。在JDK里面已经有java.util.UUID类可以生成唯一的随机数。如果希望生成的唯一数为特定的格式，那么就需要自己来生成唯一数了。生成唯一数时有两个因素是必须在考虑的：

 *				必须保证唯一，这个一般以时间为基础进行变化。
 *    			高效，当然越高效越好。
 *				有时我们希望在生成的唯一数中包含特定的内容，如把当前时间，如20110609132641，作为前缀等
 *				参考地址：https://www.cnblogs.com/ys-wuhan/p/5826837.html
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class UUIDUtils {
	private static Date date = new Date();
    private static int seq = 0;
    private static final int ROTATION = 99999;
    
    
    /**
     * 获取19位唯一数字；日期+当前时间+6位数字
     * @return
     */
    public static synchronized String counter(){
      if (seq > ROTATION) {
    	  seq = 0;
      }
      date.setTime(System.currentTimeMillis());
      String str = String.format("%1$tY%1$tm%1$td%1$tk%1$tM%1$tS%2$05d", date, seq++);
//      return Long.parseLong(str);
      return str;
    }
    
//    public static void main(String[] args) {
//        for(int i=0;i<100005;i++){
//            System.out.println(counter());
//        }
//    }

}
