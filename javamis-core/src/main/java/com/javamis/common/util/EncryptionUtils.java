/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.util;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 * @name:   加密工具类.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class EncryptionUtils {
	
	/**
     * @Description JeeIMS 使用MD5二次方加密
     * @param username     用户名
     * @param pwd          密码
     * @return             加密后的结果字符串
     */
    public static String MD5Pwd(String username, String pwd) {
        // 第一个参数:加密算法使用MD5; 第二个参数:salt盐 username + salt; 第三个参数:加密迭代次数
        String md5Pwd = new SimpleHash("MD5", pwd,ByteSource.Util.bytes(username + "salt"), 2).toHex();
        return md5Pwd;
    }
    
    //main方法用于测试使用
//    public static void main(String args[]) {
//    	System.out.println(MD5Pwd("system","123456"));
//    	System.out.println(MD5Pwd("admin","123456"));
//    }

}
