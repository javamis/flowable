/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.util;
import java.util.Date;

/**
 * @name:   日期工具类.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class DateUtils {
	/**
    *
    * @param 要转换的毫秒数
    * @return 该毫秒数转换为 * days * hours * minutes * seconds 后的格式
    * @author 
    */
   public static String formatDuring(long mss) {
       long days = mss / (1000 * 60 * 60 * 24);
       long hours = (mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
       long minutes = (mss % (1000 * 60 * 60)) / (1000 * 60);
       long seconds = (mss % (1000 * 60)) / 1000;
       
       String result = "";
       if(days !=0) {
    	   result += days + "天";
       }
       if(hours !=0) {
    	   result += hours + "小时";
       }
       if(minutes !=0) {
    	   result += minutes + "分钟";
       }
       if(seconds !=0) {
    	   result += seconds + "秒";
       }
       return result;
   }
   
   /**
    *
    * @param begin 时间段的开始
    * @param end   时间段的结束
    * @return      输入的两个Date类型数据之间的时间间格用* days * hours * minutes * seconds的格式展示
    * @author 
    */
   public static String formatDuring(Date begin, Date end) {
       return formatDuring(end.getTime() - begin.getTime());
   }
   
   //main方法用于测试使用
//   public static void main(String args[]) {
//	   long time = (long)1514339596;
//   		System.out.println(formatDuring(time));
//   }

}
