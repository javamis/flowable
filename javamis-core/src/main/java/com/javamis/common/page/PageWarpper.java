/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.page;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.javamis.common.util.HttpRequestUtil;

/**
 * @name:   分页封装类.
 * 			<p>
 *			mybatis-plus使用对象属性进行SQL操作，经常会出现对象属性非表字段的情况，忽略映射字段时可以在实体类属性上使用以下注解：
 * 			@TableField(exist = false)：表示该属性不为数据库表字段，但又是必须使用的。
 * 			@TableField(exist = true)：表示该属性为数据库表字段。
 *			如果不设置为非数据库表字段，分页查询时都将Page类的两个属性当做成继承Page子类的字段属性去查询，会报错表中无Page类内的属性字段;
 *			</p>
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class PageWarpper<T> {
	/**
	 * 数据集合
	 */
    private List<T> rows = new ArrayList<T>();
    /**
     * 数据表内的数据总条数
     */
    private int total;
	public List<T> getRows() {return rows;}
	public void setRows(List<T> rows) {this.rows = rows;}
	public int getTotal() {return total;}
	public void setTotal(int total) {this.total = total;}
    
	//获取从页面传过来的分页大小
	public int getPageSize() {return Integer.valueOf(getRequest().getParameter("limit"));}
	//获取从页面传过来的当前页码
	public int getCurrentPage() {
		int current = Integer.valueOf(getRequest().getParameter("offset"));
		int limit = Integer.valueOf(getRequest().getParameter("limit"));
		return (current / limit + 1);
	}
	//获取从页面传过来的当前列名称
	public String getSortName() {return getRequest().getParameter("sort");}
	//获取从页面传过来的排列顺序(desc;asc;)
	public String getSortOrder() {return getRequest().getParameter("order");}
	
	HttpServletRequest getRequest(){return HttpRequestUtil.getRequest();}
    
}
