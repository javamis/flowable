/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.page;

import java.util.ArrayList;
import java.util.List;

/**
 * @name:   List集合转Page（分页对象）. 
 *  		<p>
 * 			该类适用于将List集合转换为Page分页对象，（返回对象是List集合！又不得不返回前端页面是分页的情况！）
 *			注：该类不作为正常分页使用！因此无法正常使用selectPage方法返回分页Page集合.
 *			</p>
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 * @param <T> 泛型
 */
public class ListWarpper<T> {
    /***
     * 当前页
     */
    private int page = 1;
 
    /***
     * 总页数
     */
    public int totalPages = 0;
 
    /***
     * 每页数据条数
     */
    private int pageRecorders;
 
    /***
     * 总页数
     */
    private int totalRows = 0;
 
    /***
     * 每页的起始数
     */
    private int pageStartRow = 0;
 
    /***
     * 每页显示数据的终止数
     */
    private int pageEndRow = 0;
 
    /***
     * 是否有下一页
     */
    private boolean hasNextPage = false;
 
    /***
     * 是否有前一页
     */
    private boolean hasPreviousPage = false;
 
    /***
     * 数据集合
     */
    private List<T> list;
 
 
    /**
     * 通过对象集，记录总数划分
     * @param list
     * @param pageRecorders
     */
    public ListWarpper(List<T> list, int pageRecorders) {
        init(list, pageRecorders);
    }
 
    /**
     * 初始化list，并告之该list每页的记录数
     * @param list 数据集合
     * @param pageRecorders 一页显示多少条数据
     */
    public void init(List<T> list, int pageRecorders) {
        this.pageRecorders = pageRecorders;
        this.list = list;
        totalRows = list.size();
        hasPreviousPage = false;
        if ((totalRows % pageRecorders) == 0) {
            totalPages = totalRows / pageRecorders;
        } else {
            totalPages = totalRows / pageRecorders + 1;
        }
 
        if (page >= totalPages) {
            hasNextPage = false;
        } else {
            hasNextPage = true;
        }
 
        if (totalRows < pageRecorders) {
            this.pageStartRow = 0;
            this.pageEndRow = totalRows;
        } else {
            this.pageStartRow = 0;
            this.pageEndRow = pageRecorders;
        }
    }
 
 
    // 判断要不要分页
    public boolean isNext() {
        return list.size() > 5;
    }
 
    public void setHasPreviousPage(boolean hasPreviousPage) {
        this.hasPreviousPage = hasPreviousPage;
    }
 
    public String toString(int temp) {
        String str = Integer.toString(temp);
        return str;
    }
 
    public void description() {
 
        String description = "共有数据数:" + this.getTotalRows() +
 
                "共有页数: " + this.getTotalPages() +
 
                "当前页数为:" + this.getPage() +
 
                " 是否有前一页: " + this.isHasPreviousPage() +
 
                " 是否有下一页:" + this.isHasNextPage() +
 
                " 开始行数:" + this.getPageStartRow() +
 
                " 终止行数:" + this.getPageEndRow();
 
        System.out.println(description);
    }
 
    public List getNextPage() {
        page = page + 1;
 
        disposePage();
 
        System.out.println("用户调用的是第" + page + "页");
        this.description();
        return setCurrentPageNum(page);
    }
 
    /**
     * 处理分页
     */
    private void disposePage() {
 
        if (page == 0) {
            page = 1;
        }
 
        if ((page - 1) > 0) {
            hasPreviousPage = true;
        } else {
            hasPreviousPage = false;
        }
 
        if (page >= totalPages) {
            hasNextPage = false;
        } else {
            hasNextPage = true;
        }
    }
 
    public List getPreviousPage() {
 
        page = page - 1;
 
        if ((page - 1) > 0) {
            hasPreviousPage = true;
        } else {
            hasPreviousPage = false;
        }
        if (page >= totalPages) {
            hasNextPage = false;
        } else {
            hasNextPage = true;
        }
        this.description();
        return setCurrentPageNum(page);
    }
 
    /**
     * 获取第几页的内容
     *
     * @param page 当前页面
     * @return
     */
    public List<T> setCurrentPageNum(int page) {
        if(page == 0){
            this.setPage(1);
        }
        else{
            this.setPage(page);
        }
        this.disposePage();
        if (page * pageRecorders < totalRows) {
            // 判断是否为最后一页
            pageEndRow = page * pageRecorders;
            pageStartRow = pageEndRow - pageRecorders;
        } else {
            pageEndRow = totalRows;
            pageStartRow = pageRecorders * (totalPages - 1);
        }
 
        //List<T> objects = null;  //当数据为空时，则向前台返回null，导致table列表还存在已修改过的数据，如待办-当审批后刷新table列表后，该条已被审批的条目还存在table列表中!!
        List<T> objects = new ArrayList<T>();
        if (!list.isEmpty()) {
            objects = list.subList(pageStartRow, pageEndRow);
        }
        //this.description();
        return objects;
    }
 
    public List<T> getFistPage() {
        if (this.isNext()) {
            return list.subList(0, pageRecorders);
        } else {
            return list;
        }
    }
 
    public boolean isHasNextPage() {
        return hasNextPage;
    }
 
 
    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }
 
 
    public List getList() {
        return list;
    }
 
 
    public void setList(List list) {
        this.list = list;
    }
 
 
    public int getPage() {
        return page;
    }
 
 
    public void setPage(int page) {
        this.page = page;
    }
 
 
    public int getPageEndRow() {
        return pageEndRow;
    }
 
 
    public void setPageEndRow(int pageEndRow) {
        this.pageEndRow = pageEndRow;
    }
 
 
    public int getPageRecorders() {
        return pageRecorders;
    }
 
 
    public void setPageRecorders(int pageRecorders) {
        this.pageRecorders = pageRecorders;
    }
 
 
    public int getPageStartRow() {
        return pageStartRow;
    }
 
 
    public void setPageStartRow(int pageStartRow) {
        this.pageStartRow = pageStartRow;
    }
 
 
    public int getTotalPages() {
        return totalPages;
    }
 
 
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
 
 
    public int getTotalRows() {
        return totalRows;
    }
 
 
    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }
 
 
    public boolean isHasPreviousPage() {
        return hasPreviousPage;
    }
    
    
    public static void main(String args[]) {
        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");
        list.add("15");
        list.add("16");
        list.add("17");
        list.add("18");
        list.add("19");
        list.add("20");
        list.add("21");
        list.add("22");
        list.add("23");
        list.add("24");
        list.add("25");
        
        ListWarpper<String> listWarpper = new ListWarpper(list, 11);
        List<String> sublist = listWarpper.setCurrentPageNum(3);
        for(int i = 0; i < sublist.size(); i++) {
            System.out.println(sublist.get(i));
        }
    }
}
