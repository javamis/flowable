/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.message;

import lombok.Getter;

/**
 * @name:   Controller层统一返回消息类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@SuppressWarnings("unchecked")
@Getter
public class Message<T> {
	/**
	 * 消息码,如200/404/500等
	 */
	private int code;
	/**
	 * 消息内容
	 */
    private String message;
    /**
     * 根据项目实现需要输入data泛型
     */
    private T data;

    public static <T> Message<T> success(){
    	return new Message<T>((T)"");
    }
    public static <T> Message<T> success(T data){
        return new Message<T>(data);
    }
    
    public static <T> Message<T> error(int code,String msg){
        return new  Message<T>(code,msg);
        
    }
    
    private Message(T data) {
        this.code = MsgCode.SUCCESS.code;
        this.message = MsgCode.SUCCESS.message;
        if(data == null ) {
        	this.data =(T) "";
        }else {
        	this.data = data;
        }
    }
    
    public Message(int code,String message) {
        if(message == null) {
            return;
        }
        this.code = code;
        this.message = message;
    }
    
    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
