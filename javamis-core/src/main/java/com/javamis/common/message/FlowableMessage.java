/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.message;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @name:   流程结果返回值类.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlowableMessage<T> {

    public static FlowableMessage SUCCESS = new FlowableMessage();

    @ApiModelProperty("返回值状态")
    private Integer status = 0;

    @ApiModelProperty("返回数据")
    private T data;

    public FlowableMessage(T data) {
        this.data = data;
    }
}
