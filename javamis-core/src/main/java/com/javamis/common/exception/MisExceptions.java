/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.common.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.javamis.common.message.Message;
import com.javamis.common.message.MsgCode;

import org.springframework.ui.Model;
import org.springframework.http.HttpStatus;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;

/**
 * @name:   JEEIMS平台所有异常处理类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@ControllerAdvice
@SuppressWarnings("rawtypes")
public class MisExceptions {
	/**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Message notFount(RuntimeException e) {
    	return Message.error(MsgCode.ERROR.code,MsgCode.ERROR.message);
    }
    
    @ExceptionHandler(ClassCastException.class)
    public String userErrorException(ClassCastException e, Model model) {
        model.addAttribute("Flag", "true");
        model.addAttribute("FlagMsg", "请输入正确的用户名或密码");
        return "/login";
    }
    
//    @ExceptionHandler(NullPointerException.class)
//    public String nullPointerException(NullPointerException e, Model model) {
//        model.addAttribute("Flag", "true");
//        model.addAttribute("FlagMsg", "请输入正确的用户名或密码");
//        return "/login";
//    }
    
    @ExceptionHandler(value = Exception.class)
    public Message nullPointerException(Exception e, Model model) {
        model.addAttribute("Flag", "true");
        model.addAttribute("FlagMsg", "提交发生异常错误");
        return Message.error(MsgCode.ERROR.code,MsgCode.ERROR.message);
    }
    
    
    
    
    /**
	* 未知账户
	* @param e
	* @param model
	* @return
	*/
	@ExceptionHandler(value = UnknownAccountException.class)
	public String nullPointerException(UnknownAccountException e, Model model) {
		model.addAttribute("Flag", "true");
		model.addAttribute("FlagMsg", "请输入正确的用户名或密码");
		return "/login";
	}
	
	/**
	* 密码不正确
	* @param e
	* @param model
	* @return
	*/
	@ExceptionHandler(value = IncorrectCredentialsException.class)
	public String nullPointerException(IncorrectCredentialsException e, Model model) {
		model.addAttribute("Flag", "true");
		model.addAttribute("FlagMsg", "请输入正确的用户名或密码");
		return "/login";
	}
	
	/**
	     * 账户已锁定
	* @param e
	* @param model
	* @return
	*/
	@ExceptionHandler(value = LockedAccountException.class)
	public String nullPointerException(LockedAccountException e, Model model) {
		model.addAttribute("Flag", "true");
		model.addAttribute("FlagMsg", "账户已锁定");
		return "/login";
	}
	
	/**
	* 用户名或密码错误次数过多
	* @param e
	* @param model
	* @return
	*/
	@ExceptionHandler(value = ExcessiveAttemptsException.class)
	public String nullPointerException(ExcessiveAttemptsException e, Model model) {
		model.addAttribute("Flag", "true");
		model.addAttribute("FlagMsg", "用户名或密码错误次数过多");
		return "/login";
	}
	
	/**
	*用户名或密码不正确
	* @param e
	* @param model
	* @return
	*/
	@ExceptionHandler(value = AuthenticationException.class)
	public String nullPointerException(AuthenticationException e, Model model) {
		model.addAttribute("Flag", "true");
		model.addAttribute("FlagMsg", "请输入正确的用户名或密码");
		return "/login";
	}
}
