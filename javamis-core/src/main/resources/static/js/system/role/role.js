/**
 * @name    角色管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

var obj = {
    table: null,
    tableId:'tableList',
    
    /**
     * 初始化表格的列
     */
    initColumns:function () {
        var columns = [
            {title: 'Id', field: 'id', visible: true, align: 'center', valign: 'middle',width:'6%',sortable: true,cellStyle:ims.hidden,formatter: function (value, row, index) {
            	return ims.serialNumber(index,obj.tableId);
            }},
            {title: '角色名称', field: 'name', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '父角色名称', field: 'parentName', align: 'center', valign: 'middle', width:'10%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '角色类型', field: 'roleTypeName', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '更新时间', field: 'updateTime', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '角色编码', field: 'code', align: 'center', valign: 'middle', width:'10%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '备注信息', field: 'remark', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '状态', field: 'status', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {
                field: 'payStatus',
                width: '12%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    },
    
    /**
     * 搜索参数
     */
    params:function(){
    	var params = {	
    			"name":$.trim($("#name").val()),
				"roleType":$("#roleType").val(),
				"code":$.trim($("#code").val()),
				"remark":$.trim($("#remark").val()),
				"status":$.trim($("#status").val())
			};
    	return params;
    },
    
    /**
     * 搜索
     */
    search:function(){
    	obj.table.refresh({query: obj.params()});
    }
};

//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var name = row.name;
    var result = "";
    result += "<a href='javascript:;' class='btn btn-white btn-sm' onclick=\"addPage('/role/edit/" + id + "', '角色编辑')\" title='角色编辑'>编辑</a>";
    result += "<a href='javascript:;' class='btn btn-light-blue btn-sm' onclick=\"addLayer('/role/authority/" + id + "','菜单选择树')\" title='授权菜单'>分配权限</a>";
    result += "<a href='javascript:;' class='btn btn-red btn-sm' onclick=\"deletePage('/role/delete','" + id +"','"+name+"')\" title='删除'>删除</a>";
    return result;
}

$(function () {
	//1.为form页面加载性别字典值
	//loadDict();
	
	//2.初始化列表
	obj.table = new initTable(obj.tableId,"/role/listData","server",obj.initColumns());
	obj.table.setParams(obj.params());
	obj.table.init();
	
	//3.表单验证
	formValidator.init("form",formValidator.formValidate());
	
	
});
