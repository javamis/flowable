/**
 * @name    授权菜单管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

MenuTreeSave =function(){
	var zTreeOjb = $.fn.zTree.getZTreeObj("deptTree");
	var checkedNodes = zTreeObj.getCheckedNodes();
	var menuIds = "";
	for(var i=0; i<checkedNodes.length;i++){
		menuIds += checkedNodes[i].id+",";
	}
	menuIds = menuIds.substring(0,menuIds.length-1)
	if(menuIds !=""){
		var id = $("#id").val();
		new ajax("/role/authorityMenu",{id:id,menuIds:menuIds},function(data){
        	if(data.code == 200){
        		Layer.success("授权菜单成功!");
        	}else{
        		Layer.error("授权菜单失败! " + data.message);
        	}
        	setTimeout("parent.layer.close(window.parent.layerIndex)",800 );
		});
	}
}


MenuTreeClose = function(){
	parent.layer.close(window.parent.layerIndex);
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	var deptId = treeNode.id;
	if(treeNode !=undefined){}
}

$(document).ready(function(){
	//菜单树
	var zTree = new initZTree("deptTree",'/role/getMenuTree',true,"id","parentId",'name');
	zTree.setMenuTreeSelectedValue($("#menuIds").val());
	zTree.init();
});