/**
 * @name:   角色树管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

DeptTreeSave =function(){
	parent.layer.close(window.parent.layerIndex);
}

DeptTreeClose = function(){
	parent.layer.close(window.parent.layerIndex);
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	var deptId = treeNode.id;
	if(treeNode !=undefined){
		var deptName = treeNode.name;
		var curentIframeId = $("#curentIframeId").val();
		var wd = $(window.parent.document);										
		var iframeroleform = wd.find("iframe[id='"+curentIframeId+"']").contents();	
		iframeroleform.find("#parentId").val(deptId);							
		iframeroleform.find("#deptId").val(deptId);								
		iframeroleform.find("#parentName").val(deptName).change();				
		iframeroleform.find("#deptName").val(deptName).change();				
	}
}
$(document).ready(function(){
	//部门树
	var zTree = new initZTree("roleTree",'/role/getRoleTree',false,"id","parentId",'name');
	zTree.zTreeOnClick(zTreeOnClick);
	zTree.init();
});