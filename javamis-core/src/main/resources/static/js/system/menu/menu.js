/**
 * @name:   菜单管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

var obj = {
    table: null,
    deptId:0,
    isTreeClick:0,
    parentId:0,
    
    /**
     * 初始化表格的列
     */
    initColumns:function () {
        var columns = [
            {title: '菜单名称', field: 'name', align: 'center', valign: 'middle',width:'10%', sortable: true},
            {title: '菜单编号', field: 'number', align: 'center', valign: 'middle', width:'7%',sortable: true},
            {title: '请求地址', field: 'href', align: 'center', valign: 'middle', width:'10%', sortable: true},
            {title: '是否是菜单', field: 'sysMenu', align: 'center', valign: 'middle',width:'5%', sortable: true,cellStyle:ims.hidden,formatter:ims.display,formatter:function(val, obj, row, act){
            	if(val == ""|| val== undefined){
            		val = obj.sysMenu;
            	}
            	return ims.dictLabel("sys_menu",val,"未知");
			}},
            {title: '状态', field: 'isopen', align: 'center', valign: 'middle',width:'4%', sortable: true},
            {
                field: 'payStatus',
                width: '8%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    }
};



//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var name = row.name;
    var result = "";
    result += "<button href='javascript:;' class='btn btn-white btn-sm' onclick=\"addPage('/menu/edit/" + id + "', '编辑菜单')\" title='编辑菜单'>编辑</button>";
    result += "<button href='javascript:;' class='btn btn-red btn-sm' onclick=\"deletePage('/menu/delete','" + id +"','"+name+"')\" title='删除菜单'>删除</button>";
    return result;
}


/**
 * 搜索
 */
obj.search = function(isTreeClick){
	obj.table.refresh({
		query: {
			"name":$.trim($("#name").val()),
			"href":$.trim($("#href").val()),
			"sysMenu":$.trim($("#sysMenu").val()),
			"status":$.trim($("#status").val())
		  }
	});
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	obj.deptId = treeNode.id;
	obj.parentId = treeNode.parentId;
	obj.search(1);
}

$(function () {
	
	//2.菜单Table列表树
	var table = new initTreeTable("menuTable","/menu/listData",obj.initColumns());
	//table.setFoldNode(0);
    table.setId("id");
    table.setNode("id");
    table.setParentNode("parentId");
    //table.setRootNode(5);
    table.setUnfold(false);
    obj.table = table;
	obj.table.init();
	//3.表单验证
	formValidator.init("form",formValidator.formValidate());
	
	
});
