/**
 * @name:   菜单树管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

DeptTreeSave =function(){
	parent.layer.close(window.parent.layerIndex);
}

DeptTreeClose = function(){
	parent.layer.close(window.parent.layerIndex);
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	var menuId = treeNode.id;
	if(treeNode !=undefined){
		var deptName = treeNode.name;
		var curentIframeId = $("#curentIframeId").val();
		var wd = $(window.parent.document);										
		var iframeroleform = wd.find("iframe[id='"+curentIframeId+"']").contents();	
		//为 '菜单管理'模块- 新增/编辑页面 - 'parentId'属性赋值
		iframeroleform.find("#parentId").val(menuId);	
		//为 '菜单管理'模块- 新增/编辑页面 - 'parentName'属性赋值
		iframeroleform.find("#parentName").val(deptName).change();				
	}
}
$(document).ready(function(){
	var zTree = new initZTree("menuTree",'/menu/getTree',false,"id","parentId",'name');
	zTree.zTreeOnClick(zTreeOnClick);
	zTree.init();
});