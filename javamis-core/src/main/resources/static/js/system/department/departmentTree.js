/**
 * @name:   部门管理-树脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

DeptTreeSave =function(){
	parent.layer.close(window.parent.layerIndex);
}

DeptTreeClose = function(){
	parent.layer.close(window.parent.layerIndex);
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	var deptId = treeNode.id;
	if(treeNode !=undefined){
		var specifyDeptId = $("#specifyDeptId").val();
		var specifyDeptName = $("#specifyDeptName").val();
		var deptName = treeNode.briefName;
		var curentIframeId = $("#curentIframeId").val();		
		var wd = $(window.parent.document);										
		var iframeroleform = wd.find("iframe[id='"+curentIframeId+"']").contents();	
		//为 '部门管理'模块- 新增/编辑页面 - 'parentId'属性赋值
		iframeroleform.find("#parentId").val(deptId);							
		//为 '部门管理'模块- 新增/编辑页面 - 'parentName'属性赋值
		iframeroleform.find("#parentName").val(deptName).change();				
		
		//为 '用户管理'模块- 新增/编辑页面 - 'deptId'属性赋值
		iframeroleform.find("#deptId").val(deptId);	
		//为 '用户管理'模块- 新增/编辑页面 - 'deptName'属性赋值
		iframeroleform.find("#deptName").val(deptName).change();	
		//为指定部门id赋值
		iframeroleform.find("#"+specifyDeptId).val(deptId).change();
		//为指定部门name赋值
		iframeroleform.find("#"+specifyDeptName).val(deptName).change();
	}
}
$(document).ready(function(){
	var deptId = $("#deptId").val();
	//部门树
	var zTree = new initZTree("deptTree",'/department/getDeptTree/'+deptId,false,"id","parentId",'briefName');
	zTree.zTreeOnClick(zTreeOnClick);
	if(deptId == 0){deptId = 1;}
	zTree.setExpandParentId(deptId);
	zTree.init();
});