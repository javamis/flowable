/**
 * @name:   部门管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

var obj = {
    table: null,
    deptId:0,
    isTreeClick:0,
    parentId:0,
    
    /**
     * 初始化表格的列
     */
    initColumns:function () {
        var columns = [
            {title: '机构简称', field: 'briefName', align: 'center', valign: 'middle',width:'9%', sortable: true},
            {title: '机构全称', field: 'fullName', align: 'center', valign: 'middle', width:'12%',sortable: true},
            {title: '分管领导', field: 'fgLeaderName', align: 'center', valign: 'middle',width:'5%', sortable: true},
            {title: '部门负责人', field: 'leaderName', align: 'center', valign: 'middle',width:'6%', sortable: true},
            {title: '部门主任', field: 'directorName', align: 'center', valign: 'middle',width:'5%', sortable: true},
            {title: '部门副主任', field: 'deputyDirectorName', align: 'center', valign: 'middle',width:'6%', sortable: true},
            //{title: '更新时间', field: 'updateTime', align: 'center', valign: 'middle', width:'10%', sortable: true},
            {title: '排序号', field: 'code', align: 'center', valign: 'middle',width:'4%', sortable: true},
            
            {
                field: 'payStatus',
                width: '9%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    }
};

//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var fullName = row.fullName;
    var result = "";
    result += "<button href='javascript:;' class='btn btn-white btn-sm' onclick=\"addPage('/department/edit/" + id + "', '部门编辑')\" title='编辑部门'>编辑</button>";
    result += "<button href='javascript:;' class='btn btn-red btn-sm' onclick=\"deletePage('/department/delete','" + id +"','"+fullName+"')\" title='删除部门'>删除</button>";
    return result;
}

/**
 * 搜索
 */
obj.search = function(isTreeClick){
	var query = {
			"id":obj.deptId,
			"parentId":obj.parentId,
			"code":$.trim($("#code").val()),
			"briefName":$.trim($("#briefName").val()),
			"fullName":$.trim($("#fullName").val()),
			"type":$("#type").val(),
			"isTreeClick":isTreeClick
		};
	obj.table.refresh({query: query});
	
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	obj.deptId = treeNode.id;
	obj.parentId = treeNode.parentId;
	obj.search(1);
}

$(function () {
	//1.初始化部门Table列表树
	var table = new initTreeTable("departmentTable","/department/listData",obj.initColumns());
	//table.setFoldNode(0);
    table.setId("id");
    table.setNode("id");
    table.setParentNode("parentId");
    table.setRootNode(5);
    table.setUnfold(true);
    obj.table = table;
	obj.table.init();
});

$(document).ready(function(){
	//2.初始化部门树
	obj.zTree = new initZTree("deptTree",'/department/getDeptTree',false,"id","parentId",'briefName');
	obj.zTree.zTreeOnClick(zTreeOnClick);
	obj.zTree.init();
	
	//3.表单验证
	formValidator.init("form",formValidator.formValidate());//初始化表单验证控件
});
