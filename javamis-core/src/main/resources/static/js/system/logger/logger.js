/**
 * @name:   日志管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

var obj = {
    table: null,
    departmentId:0,
    departmentPid:0,
    tableId:'tableList',
    
    /**
     * 初始化表格的列
     */
    initColumns:function () {
        var columns = [
            {title: 'Id', field: 'id', visible: true, align: 'center', valign: 'middle',width:'6%',sortable: true,cellStyle:ims.hidden,formatter: function (value, row, index) {
            	return ims.serialNumber(index,obj.tableId);
            }},
            {title: '操作类型', field: 'type', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '操作人员', field: 'operUser', align: 'center', valign: 'middle', width:'10%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '访问地址', field: 'address', align: 'center', valign: 'middle', width:'8%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '业务模块', field: 'modular', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '操作时间', field: 'operTime', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '访问IP', field: 'ipAddress', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '运行耗时', field: 'runTime', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '请求参数', field: 'args', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '日志描述', field: 'details', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {
                field: 'payStatus',
                width: '12%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    },
    
    /**
     * 搜索参数
     */
    params:function(){
    	var params = {	
    			"type":$.trim($("#type").val()),
				"operCode":$.trim($("#operCode").val()),
				"modular":$.trim($("#modular").val()),
				"ipAddress":$.trim($("#ipAddress").val())
			};
    	return params;
    },
    
    /**
     * 搜索
     */
    search:function(){
    	obj.table.refresh({query: obj.params()});
    }
};

//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var userCode = row.userCode;
    var result = "";
    result += "<a href='javascript:;' class='btn btn-white btn-sm' onclick=\"addPage('/accessLog/edit/" + id + "', '查看详情')\" title='用户编辑'>详情</a>";
    result += "<a href='javascript:;' class='btn btn-red btn-sm' onclick=\"deletePage('/accessLog/delete','" + id +"','"+id+"')\" title='删除'>删除</a>";
    return result;
}


$(function () {
	//1.初始化列表
	obj.table = new initTable(obj.tableId,"/accessLog/listData","server",obj.initColumns());
	obj.table.setParams(obj.params());
	obj.table.init();
});
