/**
 * @name:   字典数据管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

var obj = {
    table: null,
    layerIndex: -1,
    
    /**
     * 初始化表格的列
     */
    initColumns:function () {
        var columns = [
            {title: 'Id', field: 'id', visible: true, align: 'center', valign: 'middle',width:'6%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '字典标签', field: 'dictLabel', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '字典数值', field: 'dictValue', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '编码', field: 'code', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '创建时间', field: 'createTime', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '备注消息', field: 'remark', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '状态', field: 'status', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {
                field: 'payStatus',
                width: '11%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    },
    
    /**
     * 提交添加用户
     */
    submit:function () {
        if(!formValidator.validate("form")){return;}
        new ajax("/dict/saveData",ims.inputKeyValue(),function(msg){
        	if(msg=="success"){
        		Layer.success("新增成功！");
    	        setTimeout("ims.closeTab()",700 );
        	}else{
        		Layer.info("新增失败！");
        	}
        });
    },
    
    /**
     * 提交修改
     */
    edit:function () {
        if(!formValidator.validate("form")){return;}
        new ajax("/dict/editData",ims.inputKeyValue(),function(msg){
        	if(msg=="success"){
        		Layer.success("修改成功！");
    	        setTimeout("ims.closeTab()",700 );
        	}else{
        		Layer.info("修改失败！");
        	}
        });
    },
    
    /**
     * 搜索
     */
    searchButton:function(){
    	var typeValue = ""
		if(typeValue ==""){
			typeValue = $("#type").val();
		}
    	obj.table.refresh({
    		query: {	
    			"type":typeValue,
    			"dictLabel":$("#dictLabel").val(),
    			"dictValue":$("#dictValue").val(),
    			"code":$("#code").val(),
    			"remark":$("#remark").val(),
    			"status":$("#status").val()
    		}
    	});
    }
};

/**
 * 刷新按钮
 */
function refreshButton(msg){
	obj.table.refresh();
	toastr.info("恭喜您，操作成功，已刷新!"); 
}

//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var dictLabel = row.dictLabel;
    var result = "";
    var dictType = $("#type").val();
    result += "<a href='javascript:;' class='btn btn-white btn-sm' onclick=\"addPage('/dict/editData/"+dictType +"/"+ id + "', '编辑数据')\" title='编辑数据'>编辑</a>";
    result += "<a href='javascript:;' class='btn btn-red btn-sm' onclick=\"deletePage('/dict/deleteDictData','" + id + "','"+dictLabel+"')\" title='删除数据'>删除</a>";
    return result;
}

$(function () {
	obj.table = new initTable("tableList","/dict/listDictData?type="+$("#type").val(),"server",obj.initColumns());
	obj.table.init();
});
