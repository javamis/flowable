/**
 * @name:   字典类型管理脚本封装
 * @version:1.0.2
 * @author: HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

var obj = {
    table: null,
    departmentId:0,
    departmentPid:0,
    tableId:'tableList',
    
    /**
     * 初始化表格的列
     */
    initColumns:function () {
        var columns = [
            {title: 'Id', field: 'id', visible: true, align: 'center', valign: 'middle',width:'6%',sortable: true,cellStyle:ims.hidden,formatter: function (value, row, index) {
            	return ims.serialNumber(index,obj.tableId);
            }},
            {title: '字典名称', field: 'name', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '字典类型', field: 'type', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '创建 / 更新时间', field: 'time', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '备注消息', field: 'remark', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '状态', field: 'status', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {
                field: 'payStatus',
                width: '11%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    },
    
    /**
     * 搜索参数
     */
    params:function(){
    	var params = {	
    			"name":$.trim($("#name").val()),
    			"type":$.trim($("#type").val()),
    			"remark":$.trim($("#remark").val()),
    			"status":$.trim($("#status").val())
			};
    	return params;
    },
    
    /**
     * 搜索
     */
    search:function(){
    	obj.table.refresh({query: obj.params()});
    }
};

//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var type = row.type;
    var name = row.name;
    var result = "";
    result += "<a href='javascript:;' class='btn btn-white btn-sm' onclick=\"addPage('/dict/editType/" + id + "', '编辑字典')\" title='编辑字典'>编辑</a>";
    result += "<a href='javascript:;' class='btn btn-light-blue btn-sm' onclick=\"addPage('/dict/listData/" + type +"', '数据字典')\" title='添加数据'>数据字典</a>";
    result += "<a href='javascript:;' class='btn btn-red btn-sm' onclick=\"deletePage('/dict/deleteDictType','" + id + "','"+name+"')\" title='删除字典'>删除</a>";
    return result;
}

$(function () {
	//初始化Table
	obj.table = new initTable(obj.tableId,"/dict/listData","server",obj.initColumns());
	obj.table.setParams(obj.params());
	obj.table.init();
	//初始化表单验证控件
	formValidator.init("form",formValidator.formValidate());
});
