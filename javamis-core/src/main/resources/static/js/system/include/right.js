/**
 * @name:   平台右侧页面脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

$(function () {
    serverUseStatistics ();
    serverUseState();
});
function serverUseStatistics () {
    var randomScalingFactor = function () { return Math.round(Math.random() * 100) };
    var doughnutData = [
        {
            value: randomScalingFactor(),
            color: "#F7464A",
            highlight: "#FF5A5E",
            label: "服务器磁盘-已使用状态"
        },
        {
            value: randomScalingFactor(),
            color: "#46BFBD",
            highlight: "#5AD3D1",
            label: "文件服务器-存储量状态"
        },
        {
            value: randomScalingFactor(),
            color: "#FDB45C",
            highlight: "#FFC870",
            label: "结构化数据-存储状态"
        },
        {
            value: randomScalingFactor(),
            color: "#949FB1",
            highlight: "#A8B3C5",
            label: "服务器-空闲率"
        }
    ];
    var ctx = document.getElementById("serverUseStatistics ").getContext("2d");
    window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, { responsive: false });
}
function serverUseState() {
    var randomScalingFactor = function () { return Math.round(Math.random() * 100) };
    var lineChartData = {
        labels: ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
        datasets: [
            {
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
            }
        ]
    }
    var ctx = document.getElementById("serverUseState").getContext("2d");
    window.myLine = new Chart(ctx).Line(lineChartData, {
        bezierCurve: false,
    });
}