/**
 * @name    用户管理脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
var obj = {
    table: null,
    departmentId:0,
    departmentPid:0,
    tableId:'tableList',
    
    /**
     * 初始化表格的列
     */
    initColumns:function () {
        var columns = [
            {title: 'Id', field: 'id', visible: true, align: 'center', valign: 'middle',width:'6%',sortable: true,cellStyle:ims.hidden,formatter: function (value, row, index) {
            	return ims.serialNumber(index,obj.tableId);
            }},
            {title: '登录账号', field: 'userCode', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '员工姓名', field: 'userName', align: 'center', valign: 'middle', width:'10%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '归属机构', field: 'deptName', align: 'center', valign: 'middle', width:'8%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '电子邮箱', field: 'email', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '手机号码', field: 'mobile', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '办公电话', field: 'phone', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '创建时间', field: 'createTime', align: 'center', valign: 'middle',width:'10%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '性别', field: 'sex', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:ims.display,formatter:function(val, obj, row, act){
            	return ims.dictLabel("user_sex",val,"未知");
			}},
            {title: '状态', field: 'status', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:ims.display,formatter:function(val, obj, row, act){
            	return ims.dictLabel("user_state",val,"未知");
			}},
            {
                field: 'payStatus',
                width: '26%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    },
    
    /**
     * 搜索参数
     */
    params:function(){
    	var params = {	
				"userCode":$.trim($("#userCode").val()),
				"userName":$.trim($("#userName").val()),
				"email":$.trim($("#email").val()),
				"phone":$.trim($("#phone").val()),
				"status":$("#status").val(),
				"sex":$("#sex").val(),
				"deptId":obj.departmentId,
				"departmentPid":$.trim(obj.departmentPid)
			};
    	return params;
    },
    
    /**
     * 搜索
     */
    search:function(){
    	obj.table.refresh({query: obj.params()});
    }
};

//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var userCode = row.userCode;
    var userName = row.userName;
    var result = "";
    result += "<a href='javascript:;' class='btn btn-white-min btn-sm' onclick=\"addPage('/user/edit/" + id + "', '用户编辑')\" title='用户编辑'>编辑</a>";
    result += "<a href='javascript:;' class='btn btn-light-blue-min btn-sm' onclick=\"addLayer('/user/assignRoles/" + id + "','分配角色选择树')\" title='添加角色'>分配角色</a>";
    result += "<a href='javascript:;' class='btn btn-blue-min btn-sm' onclick=\"obj.initPwd('" + id + "','"+userCode+"','"+userName+"')\" title='初始密码'>密码</a>";
    result += "<a href='javascript:;' class='btn btn-red-min btn-sm' onclick=\"deletePage('/user/delete','" + id +"','"+userName+"')\" title='删除'>删除</a>";
    return result;
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	obj.departmentId = treeNode.id;
	obj.departmentPid=treeNode.parentId;
	obj.search();
}


$(function () {
	//2.初始化列表
	obj.table = new initTable(obj.tableId,"/user/listData","server",obj.initColumns());
	obj.table.setParams(obj.params());
	obj.table.init();
	
	//3.初始化ztree树
	obj.zTree = new initZTree("deptTree",'/department/getDeptTree',false,"id","parentId",'briefName');
	obj.zTree.zTreeOnClick(zTreeOnClick);
	obj.zTree.init();
	
	//4.表单验证
	formValidator.init("form",formValidator.formValidate());
});
