/**
 * @name:   用户分配角色脚本封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

roleTreeSave =function(){
	var zTreeOjb = $.fn.zTree.getZTreeObj("deptTree");
	var checkedNodes = zTreeObj.getCheckedNodes();
	var roleIds = "";
	for(var i=0; i<checkedNodes.length;i++){
		roleIds += checkedNodes[i].id+",";
	}
	roleIds = roleIds.substring(0,roleIds.length-1)
	if(roleIds !=""){
		var id = $("#id").val();
		new ajax("/user/assignRolesById",{id:id,roleIds:roleIds},function(data){
        	if(data.code == 200){
        		Layer.success("分配角色成功");
        	}else{
        		Layer.error("分配角色失败! " + data.message);
        	}
        	setTimeout("parent.layer.close(window.parent.layerIndex)",800 );
		});
	}
}


roleTreeClose = function(){
	parent.layer.close(window.parent.layerIndex);
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	var deptId = treeNode.id;
	if(treeNode !=undefined){}
}

$(document).ready(function(){
	//菜单树
	var zTree = new initZTree("deptTree",'/role/getRoleTree',true,"id","parentId",'name');
	//为角色树设置初始值
	zTree.setMenuTreeSelectedValue($("#roleIds").val());
	zTree.setExpandLevel(1);
	zTree.init();
});