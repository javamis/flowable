/**
 * @name:   对bootstrap-validator校验方法通用封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

var formValidator = {
	init:function(formId,fieldsData){
		$('#'+formId).bootstrapValidator({
	        message: 'This value is not valid',
	        feedbackIcons: {
	            valid: '',
	            invalid: '',
	            validating: ''
	        },
	        fields: fieldsData
	    });
	},
	validate:function(formId){
	    $('#'+formId).bootstrapValidator('validate');
		if($('#'+formId).data('bootstrapValidator').isValid()){
			return true;
		}else{
			return false;
		}
	},
	
   /**
    * 表单验证
    */
   formValidate:function(){
	   var inputDataSets = {};
	   
	   //0.增加前端页面form 必填项验证-20211019-add-start-HuiJia
	   $(".required").each(function(){
		   var id = this.id;
		   if(id != undefined &&typeof(id) != "undefined"){
				inputDataSets[id] = {
					trigger:"change",
					validators: {
		               notEmpty: {
		                   message: '必填信息'
		               }
		           }
				};
			}
		});
	   //0.增加前端页面form 必填项验证-20211019-add-end-HuiJia
	   
		var list=document.getElementById("form").getElementsByTagName("input");
		var strData="";
		for(var i=0;i<list.length && list[i];i++){
			//1.增加前端页面form 数值(数字)类型类型验证-20211019-add-start-HuiJia
	   		if(list[i].type=="number"){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
					//inputKey +=","+id
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请输入正确的  \'数值类型\''
	   				               },
	   				               numeric: {
	   				            	   message: '请输入正确的数值类型'
	   				               }
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   				               
   				               numeric: {
   				            	   message: '请输入正确的数值类型'
   				               }
   				           }
   						};
	   				}
					
				}
	   		}
	   		//1.增加前端页面form 数值(数字)类型类型验证-20211019-add-end-HuiJia
	   		
	   		//2.增加前端页面form 邮箱验证-20211019-add-start-HuiJia
	   		if(list[i].id.indexOf("email") != -1){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
					//inputKey +=","+id
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请输入有效格式的  \'邮箱地址(如:javamis@163.com)\''
	   								},
		   				            emailAddress: {
		   	                            message: '邮箱地址格式有误，请输入有效格式的邮箱地址(如:javamis@163.com)'
		   	                        }
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   								emailAddress: {
	   	                            message: '邮箱地址格式有误，请输入有效格式的邮箱地址(如:javamis@163.com)'
	   	                        }
   				           }
   						};
	   				}
					
				}
	   		}
	   		//2.增加前端页面form 邮箱验证-20211019-add-end-HuiJia
	   		
	   		
	   		//3.增加前端页面form 密码验证-20211019-add-start-HuiJia
	   		if((list[i].type=="password" && list[i].id.indexOf("password") != -1)||(list[i].type=="password" && list[i].id.indexOf("pwd") != -1)){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
					//inputKey +=","+id
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请确保   \'两次输入的密码一致、用户名与密码不允许相同\''
	   								},
	   								identical: {
	   									field: 'confirmPassword',
	   									message: '密码与确认密码不一致，请确保密码与确认密码相同'
	   								},
	   								different: {
	   									field: 'userName',
	   									message: '密码不可与用户名相同，请重新输入密码'
	   								}
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   								identical: {
   									field: 'confirmPassword',
   									message: '密码与确认密码不一致，请确保密码与确认密码相同'
   								},
   								different: {
   									field: 'userName',
   									message: '密码不可与用户名相同，请重新输入密码'
   								}
   				           }
   						};
	   				}
				}
	   		}
	   		//3.增加前端页面form 密码验证-20211019-add-end-HuiJia
	   		
	   		
	   		//4.增加前端页面form 确认密码验证-20211019-add-start-HuiJia
	   		if((list[i].type=="password" && list[i].id.indexOf("confirmPassword") != -1)||(list[i].type=="password" && list[i].id.indexOf("confirmPwd") != -1)||(list[i].type=="password" && list[i].id.indexOf("rePwd") != -1)){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
					//inputKey +=","+id
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请确保   \'两次输入的密码一致、用户名与密码不允许相同\''
	   								},
	   								identical: {
	   									field: 'password',
	   									message: '密码与确认密码不一致，请确保密码与确认密码相同'
	   								},
	   								different: {
	   									field: 'userName',
	   									message: '密码不可与用户名相同，请重新输入密码'
	   								}
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   								identical: {
   									field: 'confirmPassword',
   									message: '密码与确认密码不一致，请确保密码与确认密码相同'
   								},
   								different: {
   									field: 'userName',
   									message: '密码不可与用户名相同，请重新输入密码'
   								}
   				           }
   						};
	   				}
				}
	   		}
	   		//4.增加前端页面form 确认密码验证-20211019-add-end-HuiJia
	   		
	   		
	   		
	   		//5.增加前端页面form 手机号码验证-20211019-add-start-HuiJia
	   		if(list[i].id.indexOf("mobile") != -1){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
					//inputKey +=","+id
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请输入正确格式的  \'电话号码\''
	   								},
	   								regexp: {
	   									regexp: /^1\d{10}$/,
		   	                            message: '手机号码格式有误，请输入有效的11位手机号码'
		   	                        }
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   								regexp: {
   									regexp: /^1\d{10}$/,
	   	                            message: '手机号码格式有误，请输入有效的11位手机号码'
	   	                        }
   				           }
   						};
	   				}
					
				}
	   		}
	   		//5.增加前端页面form 手机号码验证-20211019-add-end-HuiJia
	   		
	   		
	   		//6.增加前端页面form IP地址验证-20211019-add-start-HuiJia
	   		if(list[i].id.indexOf("ip") != -1){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请输入正确格式的  \'ip地址\''
	   								},
	   								ip: {
		   	                            message: 'ip地址格式有误，请输入正确格式的ip地址'
		   	                        }
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   								ip: {
	   	                            message: 'ip地址格式有误，请输入正确格式的ip地址'
	   	                        }
   				           }
   						};
	   				}
					
				}
	   		}
	   		//6.增加前端页面form IP地址验证-20211019-add-end-HuiJia
	   		
	   		
	   		//7.增加前端页面form 日期验证-20211019-add-start-HuiJia
	   		if(list[i].id.indexOf("date") != -1){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请输入正确格式的  \'日期\''
	   								},
	   								date: {
	   									format: 'YYYY/MM/DD',
	   									message: '日期格式有误，请输入正确格式的日期'
	   								}
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   								date: {
   									format: 'YYYY/MM/DD',
   									message: '日期格式有误，请输入正确格式的日期'
   								}
   				           }
   						};
	   				}
					
				}
	   		}
	   		//7.增加前端页面form 日期验证-20211019-add-end-HuiJia
	   		
	   		//8.增加前端页面form 身份证验证-20211019-add-start-HuiJia
	   		if(list[i].id.indexOf("date") != -1){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请输入正确格式的  \'身份证格式\''
	   								},
	   								creditCard: {
	   									message: '身份证格式有误，请输入正确格式的身份证号码'
	   								}
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   								creditCard: {
	   	                            message: '身份证格式有误，请输入正确格式的身份证号码'
	   	                        }
   				           }
   						};
	   				}
					
				}
	   		}
	   		//8.增加前端页面form 身份证验证-20211019-add-end-HuiJia
	   		
	   		
	   		//9.增加前端页面form 登录账号(账号)验证-20211019-add-start-HuiJia
	   		if(list[i].id.indexOf("userCode") != -1){
	   			var id = list[i].id;
	   			if(id != undefined &&typeof(id) != "undefined"){
	   				if(id in inputDataSets){
	   					inputDataSets[id] = {
	   							trigger:"change",
	   							validators: {
	   								notEmpty: {
	   				                   message: '必填信息,请输入新的  \'账号\''
	   								},
	   				                remote: {   
	   				                    url: 'verifyUserCode',
	   				                    message: '此账号已存在，请重新输入新的账号'
	   				                }
	   				           }
	   						};
	   				}else{
	   					inputDataSets[id] = {
   							trigger:"change",
   							validators: {
   								remote: {   
   				                    url: 'verifyUserCode',
   				                    message: '此账号已存在，请重新输入新的账号'
   				                }
   				           }
   						};
	   				}
					
				}
	   		}
	   		//9.增加前端页面form 登录账号(账号)验证-20211019-add-end-HuiJia
		}
		return inputDataSets;
   	}
}