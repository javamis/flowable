/**
 * @name:   对bootstrap-table通用封装.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

function initTable(tableId, url, pageType, data){
	loadDict();
	this.tableId = tableId;
	this.requestUrl = url;
	this.pageType = pageType;
	this.data = data;
	this.searchParams = {};
}

initTable.prototype = {
	init:function(){
		var thisObj = this;
		$('#'+this.tableId).bootstrapTable({							//初始化bootstrap-table的内容
	    	url: this.requestUrl,
	        method: 'get',												//请求方式（*）
	        dataType: "json",											//请求格式
	        dataField: 'rows',											//每页的记录行数（*）
	        striped: true,												//设置为 true 会有隔行变色效果
	        cache: false,												//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
	        undefinedText: "空",											//当数据为 undefined 时显示的字符
	        pagination: true, 											//设置为 true 会在表格底部显示分页条。
	        showToggle: "true",											//是否显示 切换试图（table/card）按钮
	        showColumns: "true",										//是否显示 内容列下拉框
	        pageNumber: 1,												//初始化加载第一页，默认第一页
	        pageSize: 11,												//每页的记录行数（*）
	        pageList: [11, 15, 30, 40],									//可供选择的每页的行数（*），当记录条数大于最小可选择条数时才会出现
	        paginationPreText: '上一页',
	        paginationNextText: '下一页',
	        search: false, 												//是否显示表格搜索,bootstrap-table服务器分页不能使用搜索功能，可以自定义搜索框，上面jsp中已经给出，操作方法也已经给出
	        searchAlign: "left",										//搜索对齐方式:左对齐
	        showColumns: false,											//是否显示 内容列下拉框
	        showToggle: false, 											//是否显示详细视图和列表视图的切换按钮
	        clickToSelect: true,  										//是否启用点击选中行
	        data_local: "zh-US",										//表格汉化
	        sidePagination: this.pageType,								//服务端处理分页
	        queryParamsType : "limit",									//设置为 ‘limit’ 则会发送符合 RESTFul 格式的参数.
	        sortOrder: "asc",                   						//排序方式
	        sortable: true,                     						//是否启用排序
	        queryParams: function (params) {							//自定义参数，这里的参数是传给后台的，我这是是分页用的
	        	var result = $.extend(thisObj.searchParams, params); 
	        	return result;				
	        },
	        responseHandler: function (res) {							
	　　　　　　	return res;
	        },
	        columns: this.data,
	        onLoadSuccess: function () {},
	        onLoadError: function () {
	            showTips("表格数据加载失败！");
	        }
	    });
	},
	
    setParams: function (param) {
        this.searchParams = param;
    },

	refresh:function (parms) {
	    if (typeof parms != "undefined") {
	    	$('#'+this.tableId).bootstrapTable('refresh', parms);
	    } else {
	    	$('#'+this.tableId).bootstrapTable('refresh');
	    }
	}
}

