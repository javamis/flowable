/**
 * @name:   对Ajax通用封装.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

$(function(){
	
	var ajax = function(url,param,callback){
		var date = new Date().getTime();
		if (url.indexOf("?") > -1) {
			url = url + "&timeStamp=" + date;
		} else {
			url = url + "?timeStamp=" + date;
		}
		$.ajax({
	        type: 'post',
	        url: url,
	        dataType: 'json',
	        async: false,
	        data: param,
	        success: function(data) {
	        	callback(data);
	        },
	        error: function(data) {
	        	callback(data);
	        }
	    });
	}
	
	window.ajax = ajax;
});