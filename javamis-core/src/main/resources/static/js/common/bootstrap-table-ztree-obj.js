/**
 * @name:   对bootstrap-table通用封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

/**
 * @param treeId	展示区域的div的id
 * @param url		请求路径
 * @param isChecked	是否展示复选框(true 、 false 分别表示 显示 、不显示 复选框或单选框)
 * @param id		子id
 * @param pId		父id
 * @param name		显示的名称字段
 * @returns
 */
function initZTree(treeId,url,isChecked,id,parentId,name){
	this.treeId = treeId;
	this.url = url;
	this.isChecked = isChecked;
	this.id = id;
	this.parentId = parentId;
	this.name = name;
	this.onClick = null;
	this.onDoubleOnClick = null;
	this.menuIds = "";
	this.level = 0;
	this.expandParentId = 1;
}

initZTree.prototype={
	init:function(){
		var parentId = this.expandParentId;
		var treeId = this.treeId;
		var setting = this.ztreeSet();
		var url = this.url;
		var menuIds = this.menuIds;
       	var resultId =menuIds.split(',');
       	var level = this.level;
		$.ajax({
	        type:"get",
	        url:url,
	        async:true,
	        success:function(result){
		       	zTreeObj = $.fn.zTree.init($("#"+treeId),setting,result);
		       	if(level ==0){
			       	zTreeObj.expandNode(zTreeObj.getNodeByParam("id",parentId,null));
		       	}else{
		       		expandLevel(zTreeObj,zTreeObj.getNodes()[0],level);
		       	}
		       	if(menuIds !=""){
		       		if(resultId.length >0){
			       		for(var i=0; i<resultId.length; i++){
				       		var node = zTreeObj.getNodeByParam("id", resultId[i]);
					       	zTreeObj.checkNode(node, true, false); 
				       	}
			       	}
		       	}
	        }
	    });
	},
	
	ztreeSet:function(){
		var zTreeOnClick = zTreeOnClick;
		var settings = {
				data: {
				  	simpleData: {
				    	enable: true,  									//true 、 false 分别表示 使用 、 不使用 简单数据模式
				    	idKey: this.id,  								//节点数据中保存唯一标识的属性名称
				    	pIdKey: this.parentId,    						//节点数据中保存其父节点唯一标识的属性名称  
				    	rootPId: -1  									//用于修正根节点父节点数据，即 pIdKey 指定的属性值
				    },
					key: {
				    	name: this.name  								//zTree 节点数据保存节点名称的属性名称  默认值："name"
					}
				},
				check:{
				    enable:this.isChecked,  							//true 、 false 分别表示 显示 、不显示 复选框或单选框
				    nocheckInherit:true  								//当父节点设置 nocheck = true 时，设置子节点是否自动继承 nocheck = true 
				},
				callback : {
			        onClick : this.onClick								// 单击事件
			    }
			};
		return settings;
	},
	
	/**
	 * 点击事件
	 */
	zTreeOnClick:function(onClick){
		this.onClick = onClick;
	},
	
	/**
	 * 获取被选择中的复选框的值
	 */
	getSelectedValue:function(){
		var zTreeOjb = $.fn.zTree.getZTreeObj(this.id);
		var checkedNodes = zTreeObj.getCheckedNodes();
		var ids = "";
		for(var i=0; i<checkedNodes.length;i++){
			ids += checkedNodes[i].id+",";
		}
		return ids.substring(0,ids.length);
	},
	
	/**
	 * 从字典数据表获取的value，为调用使用者下拉菜单设值
	 */
	setMenuTreeSelectedValue:function(menuIds){
		this.menuIds = menuIds;
	},
	
	/**
	 * 设置树展开节点的级别
	 */
	setExpandLevel(level){
		this.level = level;
	},
	
	/**
	 * @description 展开部门主键id为x的主节点.
	 * 				 如：展开部门主键id为1的主节点就是将总公司展开; 
	 * 					展开部门主键id为2的主节点就是将广东公司展开; 
	 * 					展开部门主键id为3的主节点就是将北京公司展开;
	 * 					展开部门主键id为4的主节点就是将河北公司展开; 
	 * 					展开部门主键id为20的主节点就是将天津公司展开; 
	 */
	setExpandParentId(parentId){
		this.expandParentId = parentId;
	}
}
/**
 * @author Andy
 * ZTree按节点层次展开树节点的方法-Andy
 * @param zTreeObj
 * @param node
 * @param level
 * @returns
 */
function expandLevel(zTreeObj,node,level){
	var childrenNodes = node.children;  
    for(var i=0;i<childrenNodes.length;i++){  
        zTreeObj.expandNode(childrenNodes[i], true, false, false);  
        level=level-1;  
        if(level>0){  
            expandLevel(zTreeObj,childrenNodes[i],level);  
        }  
    }  
}