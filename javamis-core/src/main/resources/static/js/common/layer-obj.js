/**
 * @name:   对layer组件通用封装.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

var Layer = {
		
	/**
	 * 普通信息提示框
	 */
	info:function(msg){
		layer.msg(msg,{icon:0});
	},
	/**
	 * 成功信息提示框
	 */
	success:function(msg){
		layer.msg(msg,{icon:1});
	},
	/**
	 * 错误信息提示框
	 */
	error:function(msg){
		layer.msg(msg,{icon:2});
	},
	
	/**
	 * 确认框
	 */
	confirm: function (message, funObj) {
		top.layer.confirm(message,{btn: ['确定', '取消']}, 
        	function (index) {
	            funObj();
	            top.layer.close(index);
	        }, 
	        function (index) {
	        	top.layer.close(index);
        });
    }
}