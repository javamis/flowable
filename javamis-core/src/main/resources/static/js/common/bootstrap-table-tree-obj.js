/**
 * @name:   对树Tree-Table表格通用封装.
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */

function initTreeTable(tableId, url, params) {
	loadDict();
    this.tableId = tableId;									//页面table的id
    this.url = url;											//请求路径
    this.params = params;									//初始化后的栏目值
    this.rootNode = null;									//设置根节点node值----可指定根节点，默认为null,"",0,"0"
    this.data = {};											//ajax的参数
    this.foldNode = 0;										//展开显示的列 
    this.id = 'id';											//选取记录返回的值
    this.node = 'node';										//用于设置父子关系
    this.parentNode = 'pNode';								//用于设置父子关系
    this.unfold = false;									//是否默认全部展开
    this.toolbarId = tableId + "Toolbar";					//工具条
    this.stripe = true;
    this.tableHeight = 460;									//默认表格高度665
    this.treeTableObj = null;								//jquery和bootstrapTreeTable绑定的对象
};

initTreeTable.prototype = {
    init: function () {
        var tableId = this.tableId;
        this.treeTableObj = $('#'+tableId).bootstrapTreeTable({
    		url: this.url,   							//请求数据的ajax的url
    		columns: this.params,						//列数组
    		id: this.id,								//选取记录返回的值
            code: this.node,							//用于设置父子关系
            parentCode: this.parentNode,				//用于设置父子关系
            type: "get", 								//请求数据的ajax类型 (注意请求方式发布是get!!!!，如果是post方式会被PreviewAop演示环境拦截导致无法获取到tree树列表Table)
            ajaxParams: this.data, 						//请求数据的ajax的data属性
            rootCodeValue: this.rootNode,				//设置根节点node值----可指定根节点，默认为null,"",0,"0"
            expandColumn: this.foldNode,				//在哪一列上面显示展开按钮,从0开始
            striped: this.stripe,   					//是否各行渐变色
            expandAll: this.unfold,  					//是否全部展开
            toolbar: "#" + this.toolbarId,				//顶部工具条
            height: this.tableHeight,
         });
        return this;
    },

    
    /**
     * 设置记录返回的id值
     */
    setId: function (id) {this.id = id;},
    /**
     * 设置记录分级的字段
     */
    setNode: function (node) {this.node = node;},
    /**
     * 设置记录分级的父级字段
     */
    setParentNode: function (parentNode) {this.parentNode = parentNode;},
    /**
     * 设置根节点node值----可指定根节点，默认为null,"",0,"0"
     */
    setRootNode: function (rootNode) {this.rootNode = rootNode;},
    /**
     * 设置渐变颜色，true:是设置渐变颜色; false:不设置渐变颜色;
     */
    setStripe:function(stripe){this.stripe = stripe;},
    /**
     * 设置在哪一列上面显示展开按钮,从0开始
     */
    setFoldNode: function (foldNode) {this.foldNode = foldNode;},
    /**
     * 设置是否默认全部展开
     */
    setUnfold: function (unfold) {this.unfold = unfold;},
    /**
     * 设置表格高度
     */
    setTableHeight: function (tableHeight) {this.tableHeight = tableHeight;},
    
    /**
     * 刷新Table表格
     */
    refresh: function (parameter) {
    	if (typeof parameter != "undefined") {
    		this.treeTableObj.bootstrapTreeTable('refresh', parameter.query);
	    } else {
	    	this.treeTableObj.bootstrapTreeTable('refresh');
	    }
    }
};


