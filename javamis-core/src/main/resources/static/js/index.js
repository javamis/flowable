﻿/**
 * @name: 	针对index页面需要的脚本通用封装
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
(function ($) {
    $.mcsLabel = {
        requestFullScreen: function () {
            var de = document.documentElement;
            if (de.requestFullscreen) {
                de.requestFullscreen();
            } else if (de.mozRequestFullScreen) {
                de.mozRequestFullScreen();
            } else if (de.webkitRequestFullScreen) {
                de.webkitRequestFullScreen();
            }
        },
        exitFullscreen: function () {
            var de = document;
            if (de.exitFullscreen) {
                de.exitFullscreen();
            } else if (de.mozCancelFullScreen) {
                de.mozCancelFullScreen();
            } else if (de.webkitCancelFullScreen) {
                de.webkitCancelFullScreen();
            }
        },
        
        /**
         * 刷新iframe
         */
        refreshTab: function () {
            var currentId = $('.middle-tabs-class').find('.active').attr('data-id');
            var target = $('.LRADMS_iframe[data-id="' + currentId + '"]');
            var url = target.attr('src');
        },
        
        /**
         * 点中选项卡时,滚动到已激活的选项卡
         */
        activeTab: function () {
            var currentId = $(this).data('id');
            if (!$(this).hasClass('active')) {
                $('.mainContent .LRADMS_iframe').each(function () {
                    if ($(this).data('id') == currentId) {
                        $(this).show().siblings('.LRADMS_iframe').hide();
                        return false;
                    }
                });
                $(this).addClass('active').siblings('.tip').removeClass('active');
                $.mcsLabel.scrollToTab(this);
            }
        },
        
        /**
         * 关闭其他选项卡
         */
        closeOtherTabs: function () {
            $('.middle-tabs-class').children("[data-id]").find('.fa-remove').parents('a').not(".active").each(function () {
                $('.LRADMS_iframe[data-id="' + $(this).data('id') + '"]').remove();
                $(this).remove();
            });
            $('.middle-tabs-class').css("margin-left", "0");
        },
        
        /**
         * 关闭选项卡菜单
         */
        closeTab: function () {
            var closeTabId = $(this).parents('.tip').data('id');
            var currentWidth = $(this).parents('.tip').width();
            if ($(this).parents('.tip').hasClass('active')) {
                if ($(this).parents('.tip').next('.tip').size()) {
                    var activeId = $(this).parents('.tip').next('.tip:eq(0)').data('id');
                    $(this).parents('.tip').next('.tip:eq(0)').addClass('active');

                    $('.mainContent .LRADMS_iframe').each(function () {
                        if ($(this).data('id') == activeId) {
                            $(this).show().siblings('.LRADMS_iframe').hide();
                            return false;
                        }
                    });
                    var marginLeftVal = parseInt($('.middle-tabs-class').css('margin-left'));
                    if (marginLeftVal < 0) {
                        $('.middle-tabs-class').animate({
                            marginLeft: (marginLeftVal + currentWidth) + 'px'
                        }, "fast");
                    }
                    $(this).parents('.tip').remove();
                    $('.mainContent .LRADMS_iframe').each(function () {
                        if ($(this).data('id') == closeTabId) {
                            $(this).remove();
                            return false;
                        }
                    });
                }
                if ($(this).parents('.tip').prev('.tip').size()) {
                    var activeId = $(this).parents('.tip').prev('.tip:last').data('id');
                    $(this).parents('.tip').prev('.tip:last').addClass('active');
                    $('.mainContent .LRADMS_iframe').each(function () {
                        if ($(this).data('id') == activeId) {
                            $(this).show().siblings('.LRADMS_iframe').hide();
                            return false;
                        }
                    });
                    $(this).parents('.tip').remove();
                    $('.mainContent .LRADMS_iframe').each(function () {
                        if ($(this).data('id') == closeTabId) {
                            $(this).remove();
                            return false;
                        }
                    });
                }
            }
            else {
                $(this).parents('.tip').remove();
                $('.mainContent .LRADMS_iframe').each(function () {
                    if ($(this).data('id') == closeTabId) {
                        $(this).remove();
                        return false;
                    }
                });
                $.mcsLabel.scrollToTab($('.tip.active'));
            }
            return false;
        },
        
        /**
         * 增加选项卡菜单
         */
        addTab: function () {
            $(".navigatebar-class>ul>li.open").removeClass("open");
            var dataId = $(this).attr('data-id');
            if (dataId != "") {
            }
            var dataUrl = $(this).attr('href');
            var urlStr = dataUrl.replace(new RegExp("/", "g"),"")
            var menuName = $.trim($(this).text());
            var flag = true;
            if (dataUrl == undefined || $.trim(dataUrl).length == 0) {
                return false;
            }
            $('.tip').each(function () {
                if ($(this).data('id') == dataUrl) {
                    if (!$(this).hasClass('active')) {
                        $(this).addClass('active').siblings('.tip').removeClass('active');
                        $.mcsLabel.scrollToTab(this);
                        $('.mainContent .LRADMS_iframe').each(function () {
                            if ($(this).data('id') == dataUrl) {
                                $(this).show().siblings('.LRADMS_iframe').hide();
                                return false;
                            }
                        });
                    }
                    flag = false;
                    return false;
                }
            });
            if (flag) {
                var str = '<a href="javascript:;" class="active tip" data-id="' + dataUrl + '">' + menuName + ' <i class="fa fa-remove"></i></a>';
                $('.tip').removeClass('active');
                
                var str1 = '<iframe class="LRADMS_iframe" id="iframe' + urlStr + '" name="iframe' + dataId + '"  width="100%" height="100%" src="' + dataUrl + '" frameborder="0" data-id="' + dataUrl + '" seamless></iframe>';
                $('.mainContent').find('iframe.LRADMS_iframe').hide().parents('.mainContent').append(str1);
                $('.son-tips .middle-tabs-class').append(str);
                $.mcsLabel.scrollToTab($('.tip.active'));
            }
            return false;
        },
        
        /**
         * 查看右侧隐藏的选项卡
         */
        scrollTabRight: function () {
            var marginLeftVal = Math.abs(parseInt($('.middle-tabs-class').css('margin-left')));
            var tabOuterWidth = $.mcsLabel.calSumWidth($(".middle-tabs").children().not(".son-tips"));
            var visibleWidth = $(".middle-tabs").outerWidth(true) - tabOuterWidth;
            var scrollVal = 0;
            if ($(".middle-tabs-class").width() < visibleWidth) {
                return false;
            } else {
                var tabElement = $(".tip:first");
                var offsetVal = 0;
                while ((offsetVal + $(tabElement).outerWidth(true)) <= marginLeftVal) {
                    offsetVal += $(tabElement).outerWidth(true);
                    tabElement = $(tabElement).next();
                }
                offsetVal = 0;
                while ((offsetVal + $(tabElement).outerWidth(true)) < (visibleWidth) && tabElement.length > 0) {
                    offsetVal += $(tabElement).outerWidth(true);
                    tabElement = $(tabElement).next();
                }
                scrollVal = $.mcsLabel.calSumWidth($(tabElement).prevAll());
                if (scrollVal > 0) {
                    $('.middle-tabs-class').animate({
                        marginLeft: 0 - scrollVal + 'px'
                    }, "fast");
                }
            }
        },
        
        /**
         * 查看左侧隐藏的选项卡
         */
        scrollTabLeft: function () {
            var marginLeftVal = Math.abs(parseInt($('.middle-tabs-class').css('margin-left')));
            var tabOuterWidth = $.mcsLabel.calSumWidth($(".middle-tabs").children().not(".son-tips"));
            var visibleWidth = $(".middle-tabs").outerWidth(true) - tabOuterWidth;
            var scrollVal = 0;
            if ($(".middle-tabs-class").width() < visibleWidth) {
                return false;
            } else {
                var tabElement = $(".tip:first");
                var offsetVal = 0;
                while ((offsetVal + $(tabElement).outerWidth(true)) <= marginLeftVal) {
                    offsetVal += $(tabElement).outerWidth(true);
                    tabElement = $(tabElement).next();
                }
                offsetVal = 0;
                if ($.mcsLabel.calSumWidth($(tabElement).prevAll()) > visibleWidth) {
                    while ((offsetVal + $(tabElement).outerWidth(true)) < (visibleWidth) && tabElement.length > 0) {
                        offsetVal += $(tabElement).outerWidth(true);
                        tabElement = $(tabElement).prev();
                    }
                    scrollVal = $.mcsLabel.calSumWidth($(tabElement).prevAll());
                }
            }
            $('.middle-tabs-class').animate({
                marginLeft: 0 - scrollVal + 'px'
            }, "fast");
        },
        
        //滚动到指定选项卡
        scrollToTab: function (element) {
            var marginLeftVal = $.mcsLabel.calSumWidth($(element).prevAll()), marginRightVal = $.mcsLabel.calSumWidth($(element).nextAll());
            var tabOuterWidth = $.mcsLabel.calSumWidth($(".middle-tabs").children().not(".son-tips"));
            var visibleWidth = $(".middle-tabs").outerWidth(true) - tabOuterWidth;
            var scrollVal = 0;
            if ($(".middle-tabs-class").outerWidth() < visibleWidth) {
                scrollVal = 0;
            } else if (marginRightVal <= (visibleWidth - $(element).outerWidth(true) - $(element).next().outerWidth(true))) {
                if ((visibleWidth - $(element).next().outerWidth(true)) > marginRightVal) {
                    scrollVal = marginLeftVal;
                    var tabElement = element;
                    while ((scrollVal - $(tabElement).outerWidth()) > ($(".middle-tabs-class").outerWidth() - visibleWidth)) {
                        scrollVal -= $(tabElement).prev().outerWidth();
                        tabElement = $(tabElement).prev();
                    }
                }
            } else if (marginLeftVal > (visibleWidth - $(element).outerWidth(true) - $(element).prev().outerWidth(true))) {
                scrollVal = marginLeftVal - $(element).prev().outerWidth(true);
            }
            $('.middle-tabs-class').animate({
                marginLeft: 0 - scrollVal + 'px'
            }, "fast");
        },
        
        /**
         * 计算元素集合的总宽度
         */
        calSumWidth: function (element) {
            var width = 0;
            $(element).each(function () {
                width += $(this).outerWidth(true);
            });
            return width;
        },
        
        /**
         * 初始化
         */
        init: function () {
            $('.menuItem').on('click', $.mcsLabel.addTab);
            $('.son-tips').on('click', '.tip i', $.mcsLabel.closeTab);
            $('.btnClose').on('click', '.tip i', $.mcsLabel.closeTab);
            $('.son-tips').on('click', '.tip', $.mcsLabel.activeTab);
            $('.tabLeft').on('click', $.mcsLabel.scrollTabLeft);
            $('.tabRight').on('click', $.mcsLabel.scrollTabRight);
            //$('.refresh-page').on('click', $.mcsLabel.refreshTab);
            $('.close-page').on('click', function () {
                $('.middle-tabs-class').find('.active i').trigger("click");
            });
            $('.close-allPage').on('click', function () {
                $('.middle-tabs-class').children("[data-id]").find('.fa-remove').each(function () {
                    $('.LRADMS_iframe[data-id="' + $(this).data('id') + '"]').remove();
                    $(this).parents('a').remove();
                });
                $('.middle-tabs-class').children("[data-id]:first").each(function () {
                    $('.LRADMS_iframe[data-id="' + $(this).data('id') + '"]').show();
                    $(this).addClass("active");
                });
                $('.middle-tabs-class').css("margin-left", "0");
            });
            $('.close-otherPage').on('click', $.mcsLabel.closeOtherTabs);
            $('.fullscreen').on('click', function () {
                if (!$(this).attr('fullscreen')) {
                    $(this).attr('fullscreen', 'true');
                    $.mcsLabel.requestFullScreen();
                } else {
                    $(this).removeAttr('fullscreen')
                    $.mcsLabel.exitFullscreen();
                }
            });
        }
    };
    
    /**
     * 首页登录
     */
    $.homePage = {
        load: function () {
            $("body").removeClass("body-class")
            $("#middle-class").find('.mainContent').height($(window).height() - 100);
            $(window).resize(function (e) {
                $("#middle-class").find('.mainContent').height($(window).height() - 100);
            });
            $(".stretch-class").click(function () {
                if (!$("body").hasClass("sidebar-collapse")) {
                    $("body").addClass("sidebar-collapse");
                } else {
                    $("body").removeClass("sidebar-collapse");
                }
            })
            $(".jeemcs-icon-class").click(function () {
                if (!$("body").hasClass("sidebar-collapse")) {
                    $("body").addClass("sidebar-collapse");
                } else {
                    $("body").removeClass("sidebar-collapse");
                }
            })
            $(".mcs-icon-class").click(function () {
                if (!$("body").hasClass("sidebar-collapse")) {
                    $("body").addClass("sidebar-collapse");
                } else {
                    $("body").removeClass("sidebar-collapse");
                }
            })
            $(window).load(function () {
                window.setTimeout(function () {
                    $('#page-loader').fadeOut();
                }, 300);
            });
        },
        jsonWhere: function (data, action) {
            if (action == null) return;
            var reval = new Array();
            $(data).each(function (i, v) {
                if (action(v)) {
                    reval.push(v);
                }
            })
            return reval;
        },
        
        
        /**
         * 加载菜单栏
         */
        loadMenu: function () {
        	var data;
        	$.ajax({
                type:"GET",
                url:"/menus",
                data:{roleIds:$("#roleIds").val()},
                dataType:"json", 
                async:false,
                success:function(result){
                   if(result.length>0){
                	   data = result;
                	   console.log(data);
                   }else{
                   }
                }
        	}); 
        	
            var _html = "";
            $.each(data, function (i) {
                var row = data[i];
                if (row.F_ParentId == "0") {
                    if (i == 0) {
                        _html += '<li class="treeview active">';
                    } else {
                        _html += '<li class="treeview">';
                    }
                    _html += '<a href="#">'
                    _html += '<i class="' + row.F_Icon + '"></i><span>' + row.F_FullName + '</span><i class="fa fa-angle-left pull-right"></i>'
                    _html += '</a>'
                    var childNodes = $.homePage.jsonWhere(data, function (v) {
                    	return v.F_ParentId == row.F_ModuleId 
                    	}
                    );
                    if (childNodes.length > 0) {
                        _html += '<ul class="left-navigate">';
                        $.each(childNodes, function (i) {
                            var subrow = childNodes[i];
                            var subchildNodes = $.homePage.jsonWhere(data, function (v) { return v.F_ParentId == subrow.F_ModuleId });
                            _html += '<li>';
                            if (subchildNodes.length > 0) {
                                _html += '<a href="#"><i class="' + subrow.F_Icon + '"></i>' + subrow.F_FullName + '';
                                _html += '<i class="fa fa-angle-left pull-right"></i></a>';
                                _html += '<ul class="left-navigate">';
                                $.each(subchildNodes, function (i) {
                                    var subchildNodesrow = subchildNodes[i];
                                    //_html += '<li><a class="menuItem" data-id="' + subrow.F_ModuleId + '" href="' + subrow.F_UrlAddress + '"><i class="' + subchildNodesrow.F_Icon + '"></i>' + subchildNodesrow.F_FullName + '</a></li>';
                                    _html += '<li><a class="menuItem" data-id="' + subchildNodesrow.F_ModuleId + '" href="' + subchildNodesrow.F_UrlAddress + '"><i class="' + subchildNodesrow.F_Icon + '"></i>' + subchildNodesrow.F_FullName + '</a></li>';
                                });
                                _html += '</ul>';

                            } else {
                                _html += '<a class="menuItem" data-id="' + subrow.F_ModuleId + '" href="' + subrow.F_UrlAddress + '"><i class="' + subrow.F_Icon + '"></i>' + subrow.F_FullName + '</a>';
                            }
                            _html += '</li>';
                        });
                        _html += '</ul>';
                    }
                    _html += '</li>'
                }
            });
            $("#menu-class").append(_html);
            
            
            /**
             * 点击一级菜单目录时，展开二级菜单目录
             */
            $("#menu-class li a").click(function () {
                var d = $(this), e = d.next();
                if (e.is(".left-navigate") && e.is(":visible")) {
                    e.slideUp(500, function () {
                        e.removeClass("menu-open")
                    }),
                    e.parent("li").removeClass("active")
                } else if (e.is(".left-navigate") && !e.is(":visible")) {
                    var f = d.parents("ul").first(),
                    g = f.find("ul:visible").slideUp(500);
                    g.removeClass("menu-open");
                    var h = d.parent("li");
                    e.slideDown(500, function () {
                        e.addClass("menu-open"),
                        f.find("li.active").removeClass("active"),
                        h.addClass("active");

                        var _height1 = $(window).height() - $("#menu-class >li.active").position().top - 41;
                        var _height2 = $("#menu-class li > ul.menu-open").height() + 10
                        if (_height2 > _height1) {
                            $("#menu-class >li > ul.menu-open").css({
                                overflow: "auto",
                                height: _height1
                            })
                        }
                    })
                }
                e.is(".left-navigate");
            });
        }
    };
    $(function () {
        $.homePage.load();
        $.homePage.loadMenu();
        $.mcsLabel.init();
    });
})(jQuery);