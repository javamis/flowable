<%/* %>
   	 按钮标签参数说明:
    class  : 按钮上的样式
    onclick: 点击按钮触发的方法
    id     : 按钮id
    name   : 按钮name
    icon   : 按钮上的图标
<%*/ %>
<%if(isNotEmpty(iconLocation) && iconLocation == 'icon-after'){ %>
<button 
	<%if(isNotEmpty(class)){ %>
		class="${class}" 
	<%} %>
	<%if(isNotEmpty(onclick)){ %>
		onclick="${onclick}"
	<%} %>
	
	<%if(isNotEmpty(id)){ %>
		id="${id}"
	<%} %>
	 >
	<%if(isNotEmpty(name)){ %>
    	${name}
	<%} %>
	<%if(isNotEmpty(icon)){ %>
    	<i class="fa ${icon}"></i>
	<%} %>
    
</button>

<%}else{%>
<button class="${class}" 
	<%if(isNotEmpty(onclick)){ %>
		onclick="${onclick}"
	<%}  %>
	<%if(isNotEmpty(id)){ %>
		id="${id}"
	<%} %>
	>
	<%if(isNotEmpty(icon)){ %>
    	<i class="fa ${icon}"></i>
	<%} %>
	<%if(isNotEmpty(name)){ %>
    	${name}
	<%} %>
</button>
<%}%>


