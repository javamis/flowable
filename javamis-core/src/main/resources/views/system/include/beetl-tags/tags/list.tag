<% /* %>
     列表中搜索框标签参数说明:
	type     :input输入框类型
    id       : input输入框id
    style    : 输入框的样式
    value    : 输入框value值
    readonly : 输入框只读属性
    clickFun  : 点击按钮触发的方法
    iconClass: 输入框上的图标样式
    icon     : 输入框上的图标
<%*/%>
<input  id="${id}" name="${id}"
       <% if(isNotEmpty(value)){ %>
            value="${value}"
       <%} %>
      <% if(isNotEmpty(type)){ %>
       		type="${type}"
      <% }else{ %>
       		type="text"
       <% } %>
       <%if(isNotEmpty(class)){%>
       		class="form-control ${class}" 
       	<%}else{%>
       		class="form-control search-input"
       	<%} %>
      <% if(isNotEmpty(readonly)){ %>
            readonly="${readonly}"
       <% } %>
       <%if(isNotEmpty(clickFun)){ %>
            onclick="${clickFun}"
       <%} %>
       <%if(isNotEmpty(style)){ %>
            style="${style}"
       <% } %>
       <%if(isNotEmpty(disabled)){ %>
            disabled="${disabled}"
       <% } %>
       <%if(isNotEmpty(style)){ %>
       		style="${style}"
       <%} %>
 />
<span class="input-group-btn">
 	<a id="inputButton" 
 		<%if(isNotEmpty(iconClass)){%>
 			class="btn ${iconClass}" 
 		<%}else{%>
 			class="btn btn-list" 
 		<%}%>
 		<%if(isNotEmpty(onclick)){%>
            onclick="${onclick}"
       	<%}%>
 	 >
 		<i 
	 		<%if(isNotEmpty(icon)){%>
	 			class="fa ${icon}"
	 		<%}else{%>
	 			class="fa fa-file-text-o"
	 		<%}%>
 		>
 		</i>
 	</a>
</span>

