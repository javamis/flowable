###  技术交流： 
- QQ 交流：763236277
- QQ 入群：327773608
- 官方网站：http://www.javamis.com
-  **在线文档** ：http://www.javamis.com/doc/origin.html




####  JavaMis介绍
企业级流程中心，基于Flowable封装，使得更符合国内政务办公，技术包括：SpringBoot框架、MyBatis、MyBatisplus分页、Flowable工作流、Beetl模板引擎、Shiro权限框架、Bootstrap前端框架、Layer弹层组件等。

javaMis是企业级后台开发平台、通用型后台管理系统快速开发平台，界面大方、开箱即用；平台将陆续推出通过使用javaMis平台开发出的优秀系统和案例；欢迎个人开发者和企业使用！


###  在线演示
- 地址：http://demo.javamis.com/
- 账号：system
- 密码：123456


### 软件架构选型   

- 后台框架：Spring Boot、Apache Shiro、Thymeleaf、Framework、AspectJ面向切面AOP框架
- 持 久 层：Apache MyBatis、MyBatis-plus等

- 前端框架：Bootstrap、Bootstrap-Validator、Beetl超高性能模板引擎
- 前端组件：jQuery、Bootstrap-table、layer、zTree树、jQuery Validation、Toastr消息提示框

- 工作流：Flowable、Flowable-modeler在线流程设计器、BPMN 规范、中国式工作流程、驳回、自由流等

- 浏览器： 支持IE8以上版本及所有现代浏览器，如：火狐浏览器、360浏览器、谷歌浏览器、QQ浏览器等


#### 使用说明

- 发展历程：http://www.javamis.com/doc/origin.html
- 技术选型：http://www.javamis.com/doc/technology.html
- 目录结构：http://www.javamis.com/doc/platformStructure.html
- YML配置：http://www.javamis.com/doc/ymlConfig.html
- Beetl配置：http://www.javamis.com/doc/beetlConfig.html
- 环境配置：http://www.javamis.com/doc/environmentConfig.html
- 快速开始：http://www.javamis.com/doc/quickStart.html
- intellij开始：http://www.javamis.com/doc/quickStart-intellij.html
- 标签规范：http://www.javamis.com/doc/formComponent.html
- 脚本规范：http://www.javamis.com/doc/javascriptStandard.html
- 表单规范：http://www.javamis.com/doc/developmentStandard.html
- 后端规范：http://www.javamis.com/doc/serverStandard.html
- 开发示例1：http://www.javamis.com/doc/serverStandard.html
- 开发示例2：http://www.javamis.com/doc/absentDemo.html



#### 功能列表介绍

- --组织管理
- ----用户管理
- ----部门管理
- ----角色管理
-
- --系统管理
- ----菜单管理
- ----字典管理
-
- --监控管理
- ----系统日志
- 
- --工作流程
- ----模型设计
- ----模型管理
- ----流程管理
-
- --协同办公
- ----待办事项
- ----已办事项
- ----已发事项
-
- --开发示例
- ----固定资产开发-示例
- ----请假单开发-示例
- 
- --持续更新...



#### 更多介绍
    JavaMis快速开发平台，是通用型后台管理系统快速开发平台，界面大方、开箱即用，平台将陆续推出通过使用javaMis平台开发出的优秀系统和案例，欢迎个人开发者和企业使用~

一.  下面是  :point_right: JavaMis快速开发平台首页介绍：   
JavaMis平台已经封装了常用的基础模块（如：用户管理/角色管理/数据字典等，如：下图所示）和技术细节，JavaMis快速开发平台能够帮助初级和中级的研发人员快速的开发出复杂的业务系统，让研发人员的更多的精力专注自已本身的业务系统上，从而节省人力成本，缩短项目研发周期，提高软件安全质量。
![输入图片说明](http://www.javamis.com/doc/introduce_files/javamis-index-2.png)


二.  下面是 :point_right: 工作流 - 已发事件列表介绍：
![输入图片说明](http://www.javamis.com/doc/introduce_files/flowable-2.png)
                               

三.  下面是 :point_right: 工作流 - 审批流程图和审批环节审批意见介绍：
![输入图片说明](http://www.javamis.com/doc/introduce_files/flowable-1.png)
                           


四.  下面是 :point_right: 查询列表 - 代码块介绍

![输入图片说明](http://www.javamis.com/doc/introduce_files/select-list-2.png)


