var obj = {
    table: null,
    departmentId:0,
    departmentPid:0,
    tableId:'tableList',
    
    /**
     * 初始化表格的列
     */
    initColumns:function () {
        var columns = [
            {title: 'Id', field: 'id', visible: true, align: 'center', valign: 'middle',width:'3%',sortable: true,cellStyle:ims.hidden,formatter: function (value, row, index) {
            	return ims.serialNumber(index,obj.tableId);
            }},
            {title: '请假标题', field: 'titleName', align: 'left', valign: 'middle', width:'30%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '请假类型', field: 'type', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:function(val, obj, row, act){
            	return ims.dictLabel("leave_type",val,"未知");
			}},
            {title: '请假天数', field: 'days', align: 'center', valign: 'middle', width:'7%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '请假原因', field: 'reason', align: 'center', valign: 'middle', width:'15%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '领导意见', field: 'leaderOpinion', align: 'center', valign: 'middle',width:'16%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {
                field: 'payStatus',
                width: '19%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    },
    
    /**
     * 搜索参数
     */
    params:function(){
    	var params = {	
    			"type":$("#type").val(),
				"days":$("#days").val(),
				"reason":$.trim($("#reason").val()),
				"leaderOpinion":$.trim($("#leaderOpinion").val()),
				"status":$("#status").val()
			};
    	return params;
    },
    
    /**
     * 搜索
     */
    search:function(){
    	obj.table.refresh({query: obj.params()});
    }
};

//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var userCode = row.userCode;
    var userName = row.userName;
    var procDefId = row.procDefId;
    var procInsId = row.procInsId;
    var processIsFinished = row.processIsFinished;
    var result = "";
    result += "<a href='javascript:;' class='btn btn-white btn-sm' onclick=\"addPage('/absent/edit/" + id + "', '请假单编辑')\" title='请假单编辑'>编辑</a>";
    result += "<a href='javascript:;' class='btn btn-red btn-sm' onclick=\"deletePage('/absent/delete','" + id +"','"+userName+"')\" title='删除'>删除</a>";
    if(processIsFinished == "10002"){//10002：已完结；当已完结时为hisInsId赋值procInsId；当未完结时为instanceId赋值procInsId; 如果不加判断未完结的流程图显示不正确(偶尔也会显示已完结)!!!
    	result += "<a href='javascript:;' class='btn btn-blue-width27-min btn-sm' onclick=\"addLayerCustom('/flowableOffice/taskDiagram?definitionId=" +procDefId + "&hisInsId=" + procInsId +"', '查看流程图','1280','590')\" title='查看流程图'>流程追踪</a>";
    }else{
    	result += "<a href='javascript:;' class='btn btn-light-blue-width27-min btn-sm' onclick=\"addLayerCustom('/flowableOffice/taskDiagram?definitionId=" +procDefId + "&instanceId=" + procInsId +"', '查看流程图','1280','590')\" title='查看流程图'>流程追踪</a>";
    }
    return result;
}

//单击事件，向后台发起请求
function zTreeOnClick(event, treeId, treeNode) {
	obj.departmentId = treeNode.id;
	obj.departmentPid=treeNode.parentId;
	obj.search();
}


$(function () {
	//1.为form页面加载性别字典值
	//loadDict();
	//2.初始化列表
	obj.table = new initTable(obj.tableId,"/absent/listData","server",obj.initColumns());
	obj.table.setParams(obj.params());
	obj.table.init();
	//3.表单验证
	formValidator.init("form",formValidator.formValidate());
	
	
	//4.时间函数-用于在absent/form和edit页面显示当前系统时间，该函数不是框架必须使用，所以可保留亦可删除-start-HuiJia
	var timerDiv=document.querySelector('.timer');//获取timer的dom对象
	function times(){//时间的方法函数
		//创建一个当前日期对象
		var d=new Date();
		var y=d.getFullYear(); //年
		var m=d.getMonth()+1;  //月
		var dd=d.getDate();  //日
		var hh=d.getHours();  //时
		var min=d.getMinutes();  //分
		var ss=d.getSeconds();  //秒
		var timeFormat=y+'年'+m+'月'+dd+'日'+hh+'时'+min+'分'+ss+'秒';
		timerDiv.innerHTML=timeFormat;  //通过js插到div内容里面去
	}
	times(); //定时器里面的是一秒钟之后才会执行的，所以呢，如果不调用这个函数的话，那一秒钟之前他是没有数字显示的，所以必须要在一秒钟之前的时候调用这个函数，那它才会正常显示，那一秒钟之后的话，它就执行那个定时器里面的函数  //使其刷新前不留空白
	setInterval(times,1000); //定时器 //定时器里的参数单位是毫秒，1秒=1000毫秒
	//时间函数-用于在absent/form和edit页面显示当前系统时间，该函数不是框架必须使用，所以可保留亦可删除-end-HuiJia
});
