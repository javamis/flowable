var obj = {
    table: null,
    tableId:'tableList',
    
    /**
     * 初始化表格
     */
    initColumns:function () {
        var columns = [
            {title: '序号', field: 'id', visible: true, align: 'center', valign: 'middle',width:'2%',sortable: true,cellStyle:ims.hidden,formatter: function (value, row, index) {
            	return ims.serialNumber(index,obj.tableId);
            }},
            {title: '资产编号', field: 'assCode', align: 'center', valign: 'middle',width:'5%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '资产名称', field: 'assDeviceName', align: 'center', valign: 'middle', width:'12%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '品牌', field: 'assBrand', align: 'center', valign: 'middle', width:'3%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '型号', field: 'assSpeType', align: 'center', valign: 'middle', width:'3%',sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '分类', field: 'assType', align: 'center', valign: 'middle', width:'3%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '使用部门', field: 'pmsPurDepartName', align: 'center', valign: 'middle',width:'7%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '使用人', field: 'assCustodiansName', align: 'center', valign: 'middle',width:'4%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '领用时间', field: 'assStartDate', align: 'center', valign: 'middle',width:'5%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {title: '存放地点', field: 'assPlace', align: 'center', valign: 'middle',width:'6%', sortable: true,cellStyle:ims.hidden,formatter:ims.display},
            {
                field: 'payStatus',
                width: '9%',
                title: '操作',
                formatter: actionFormatter,
                align:'center'
            }
        ];
        return columns;
    },
    
    /**
     * 搜索参数
     */
    params:function(){
    	var params = {	
    			"assCode":$.trim($("#assCode").val()),
				"assDeviceName":$.trim($("#assDeviceName").val()),
				"assType":$.trim($("#assType").val()),
				"assCustodiansName":$.trim($("#assCustodiansName").val())
			};
    	return params;
    },
    
    /**
     * 搜索
     */
    search:function(){
    	obj.table.refresh({query: obj.params()});
    }
};

//操作栏的格式化
function actionFormatter(value, row, index) {
    var id = row.id;
    var name = row.name;
    var procDefId = row.procDefId;
    var procInsId = row.procInsId;
    var processIsFinished = row.processIsFinished;
    var result = "";
    result += "<a href='javascript:;' class='btn btn-white btn-sm' onclick=\"addPage('/assStorage/edit/" + id + "', '资产编辑')\" title='资产编辑'>编辑</a>";
    result += "<a href='javascript:;' class='btn btn-red btn-sm' onclick=\"deletePage('/assStorage/delete','" + id +"','"+name+"')\" title='删除'>删除</a>";
    if(processIsFinished == "10002"){//10002：已完结；当已完结时为hisInsId赋值procInsId；当未完结时为instanceId赋值procInsId; 如果不加判断未完结的流程图显示不正确(偶尔也会显示已完结)!!!
    	result += "<a href='javascript:;' class='btn btn-blue-width27-min btn-sm' onclick=\"addLayerCustom('/flowableOffice/taskDiagram?definitionId=" +procDefId + "&hisInsId=" + procInsId +"', '查看流程图','1280','590')\" title='查看流程图'>流程追踪</a>";
    }else{
    	result += "<a href='javascript:;' class='btn btn-light-blue-width27-min btn-sm' onclick=\"addLayerCustom('/flowableOffice/taskDiagram?definitionId=" +procDefId + "&instanceId=" + procInsId +"', '查看流程图','1280','590')\" title='查看流程图'>流程追踪</a>";
    }
    return result;
}

$(function () {
	//1、初始化Table表格
	obj.table = new initTable(obj.tableId,"/assStorage/listData","server",obj.initColumns());
	obj.table.setParams(obj.params());
	obj.table.init();
	//2、初始化表单验证控件
	formValidator.init("form",formValidator.formValidate());
	//为form页面加载性别字典值
	//loadDict();
});
