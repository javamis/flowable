package com.javamis.config.generateCode;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @name:   Mybatis-Plus代码生成器(此类只用作代码生成)
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public class MybatisPlusGen {

    // 修改此变量生成代码
    private static final String file = "D:\\MybatisPlusCode";
    // 固定参数
    private static DbType dbType;
    private static String driverName;
    private static String url;
    private static String name;
    private static String password;

    static void init() {
        Properties properties = new Properties();
        try (InputStream is = new FileInputStream(new File(file + "\\conf.txt"))) {
            properties.load(is);

            dbType = DbType.valueOf(properties.getProperty("dbType"));
            driverName = properties.getProperty("driverName");
            url = properties.getProperty("url");
            name = properties.getProperty("username");
            password = properties.getProperty("password");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public static void main(String[] args) {
        // 属性不方便公开
        init();

        AutoGenerator ag = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(file);
        gc.setFileOverride(true);
        gc.setActiveRecord(false);// 不需要ActiveRecord特性的请改为false
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(false);// XML columList
        //作者
        gc.setAuthor("HX");

        //自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setControllerName("%sController");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        ag.setGlobalConfig(gc);

        //数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(dbType);
        dsc.setDriverName(driverName);
        dsc.setUsername(name);
        dsc.setPassword(password);
        dsc.setUrl(url);
        ag.setDataSource(dsc);

        //需要生成的表
        String[] tables = new String[]{"TM_APPLICATION"};

        //项目包名
        String mybatisPackage = "com.demo";

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        //此处可以修改为表前缀
        strategy.setTablePrefix(new String[]{"TM_"});
        //表名生成策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        //需要生成的表
        strategy.setInclude(tables);
        strategy.setSuperServiceClass(null);
        strategy.setSuperServiceImplClass(null);
        strategy.setSuperMapperClass(null);
        // 配置 @RestController
        strategy.setRestControllerStyle(true);
        // 设置 superEntityClass
        strategy.setSuperEntityClass(BaseEntity.class.getName());
        ag.setStrategy(strategy);

        //包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(mybatisPackage);
        pc.setController("controller");
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setMapper("mapper");
        pc.setEntity("entity");
        pc.setXml("xml");
        ag.setPackageInfo(pc);

        //指定自定义模板路径, 位置：/resources/templates/entity2.java.ftl(或者是.vm)
        //注意不要带上.ftl(或者是.vm), 会根据使用的模板引擎自动识别
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setEntity("/entity.java");
        templateConfig.setXml("/mapper.xml");
        //配置自定义模板
        ag.setTemplate(templateConfig);

        // 执行生成
        ag.execute();
    }
}

