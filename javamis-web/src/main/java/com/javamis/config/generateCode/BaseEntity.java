package com.javamis.config.generateCode;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @name:   Base实体类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
public class BaseEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 功能说明: 状态 [ A - ACTIVE(可用状态) | I - INACTIVE(无效状态) ]
     */
    @TableField("STATUS")
    private String status;

    /**
     * 功能说明: createDate
     */
    @TableField("CREATE_DATE")
    private Date createDate;

    /**
     * 功能说明: createBy
     */
    @TableField("CREATE_BY")
    private String createBy;

    /**
     * 功能说明: updateDate
     */
    @TableField("UPDATE_DATE")
    private Date updateDate;

    /**
     * 功能说明: updateBy
     */
    @TableField("UPDATE_BY")
    private String updateBy;
}
