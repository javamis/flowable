/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.flowable.engine.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javamis.common.logger.Logger;
import com.javamis.common.logger.LoggerModule;
import com.javamis.common.logger.LoggerType;
import com.javamis.common.message.Message;
import com.javamis.common.page.PageWarpper;
import com.javamis.common.util.UUIDUtils;
import com.javamis.modular.demo.dao.AbsentMapper;
import com.javamis.modular.flowable.entity.Flowable;
import com.javamis.modular.demo.entity.Absent;
import com.javamis.modular.demo.service.AbsentService;
import com.javamis.modular.system.dao.DictDataMapper;

/**
 * @name:   请假单Controller层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Controller
@RequestMapping("/absent")
@SuppressWarnings("rawtypes")
public class AbsentController {
	
	@Autowired
	private AbsentMapper leaveMapper;
	@Autowired
	DictDataMapper dictDataMapper;
	@Autowired
    protected RepositoryService repositoryService;
	@Autowired
	private AbsentService absentService;
	
    /**
     *请假单列表
     */
    //@RequiresPermissions("user:list")
    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String list(Model model) {
    	return "demo/absent/list";
    }
    
    @RequestMapping(value = "listData")
    @ResponseBody
    @SuppressWarnings("unlikely-arg-type")
    public PageWarpper<Absent> listData(Absent leave){
    	PageWarpper<Absent> pageWarpper = new PageWarpper<Absent>();
    	IPage<Absent> page = new Page<>(pageWarpper.getCurrentPage(), pageWarpper.getPageSize());
    	QueryWrapper<Absent> queryWrapper = new QueryWrapper<>();
    	Map<String, Object> queryMap = leave.setData(leave);
	 	String sort = pageWarpper.getSortName();
	 	String sortOrder =  pageWarpper.getSortOrder();
	 	if(queryMap.isEmpty() && sort == null) {
    		leaveMapper.selectPage(page, null);
    	}if(queryMap.isEmpty() && sort != null) {
    		if(sortOrder.equals("desc")) {
    			queryWrapper.orderByDesc(sort);
    		}if(sortOrder.equals("asc")) {
    			queryWrapper.orderByAsc(sort);
    		}
    		leaveMapper.selectPage(page, queryWrapper);
    	}if(!queryMap.isEmpty()) {
    		queryWrapper.allEq(queryMap);
    		leaveMapper.selectPage(page, queryWrapper);
    	}
    	pageWarpper.setTotal(new Long(page.getTotal()).intValue());
    	List<Absent> absents = page.getRecords();
    	List<Absent> absentsWarp =  absentService.listData(absents);
    	//pageWarpper.setRows(absents);
    	pageWarpper.setRows(absentsWarp);
    	return  pageWarpper;
    }
    
    /**
     * 请假单页面
     * @param model
     * @return
     */
    //@RequiresPermissions("user:show")
    @RequestMapping(value = "form")
    public String form() {
    	return "demo/absent/form";
    }
    
    /**
     * 添加流程表单
     */
    @RequestMapping("/save")
    @ResponseBody
    @Logger(detail = "添加请假单-流程表单",level = 3,unit = LoggerModule.LEAVE,type = LoggerType.INSERT)
    public Message<String> save(@Valid Absent absent,Flowable flowable, HttpServletRequest request) {
    	absent.setId(UUIDUtils.counter());
    	absent.setOperTime(new Date());
        this.leaveMapper.insert(absent);
        return absentService.save(absent,flowable);
    }
    
    /**
     * 根据请假单id，修改请假单信息
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="edit/{id}")
    public String edit(@PathVariable String id, Absent absent,Model model) {
    	Absent leaveObj = leaveMapper.selectById(id);
    	absent.setType(leaveObj.getType());
    	absent.setDays(leaveObj.getDays());
    	absent.setReason(leaveObj.getReason());
    	absent.setLeaderOpinion(leaveObj.getLeaderOpinion());
    	Absent leave = absentService.edit(absent);
    	model.addAttribute("leave",leave);
    	return "demo/absent/edit";
    }
    
    /**
     * 修改请假单
     */
    @RequestMapping("/edit")
    @ResponseBody
    public Message<String> edit(@Valid Absent absent,BindingResult result) {
        this.leaveMapper.updateById(absent);
        absentService.edit(absent, absent.getIsAgree());
        return Message.success("修改请假单成功啦!");
    }

    /**
     * 删除请假单
     */
    @RequestMapping(value="delete")
    @ResponseBody
    @Logger(unit = LoggerModule.USER,type = LoggerType.DELETE)
    public Message delete(@RequestParam Integer id) {
        this.leaveMapper.deleteById(id);
        return Message.success();
    }
}
