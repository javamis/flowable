/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.demo.dao;
import org.apache.ibatis.annotations.Select;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javamis.modular.demo.entity.AssStorage;
import com.javamis.modular.flowable.entity.FlowableModel;

/**
 * @name:   用户dao层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface AssStorageMapper extends BaseMapper<AssStorage> {
	
	/**
	 * @Description 根据id查询act_de_model表
	 * @param id
	 * @return
	 */
	@Select("SELECT * from act_de_model WHERE id = #{id}")
	public FlowableModel selectActDeModelById(String id);
}
