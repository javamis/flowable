package com.javamis.modular.demo.service.impl;

import static com.javamis.config.shiro.ShiroUtils.getShiroInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.javamis.common.message.Message;
import com.javamis.common.util.JsonTransferUtils;
import com.javamis.common.util.UUIDUtils;
import com.javamis.config.shiro.ShiroInfo;
import com.javamis.modular.demo.dao.AbsentMapper;
import com.javamis.modular.demo.entity.Absent;
import com.javamis.modular.demo.service.AbsentService;
import com.javamis.modular.flowable.dao.FlowableModelMapper;
import com.javamis.modular.flowable.dao.FlowableOfficeMapper;
import com.javamis.modular.flowable.entity.FlowFormBusiness;
import com.javamis.modular.flowable.entity.Flowable;
import com.javamis.modular.flowable.entity.FlowableMisModelForm;
import com.javamis.modular.flowable.entity.flowable.FlowableCompleteTask;
import com.javamis.modular.flowable.entity.flowable.FlowableStartProcessInstance;
import com.javamis.modular.flowable.service.FlowTaskService;
import com.javamis.modular.flowable.service.IFlowableProcessInstanceService;
import com.javamis.modular.flowable.service.IFlowableTaskService;

/**
 * @name:   请假单类接口实现类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Service
public class AbsentServiceImpl extends ServiceImpl<AbsentMapper, Absent> implements AbsentService  {
	@Autowired
    private FlowableOfficeMapper flowableOfficeMapper;
	@Autowired
	private FlowTaskService flowTaskService;
	@Autowired
    protected RepositoryService repositoryService;
	@Autowired
    private IFlowableProcessInstanceService iFlowableProcessInstanceService;
	@Autowired
	private FlowableModelMapper flowableModelMapper;
	@Autowired
	private FlowTaskService actTaskService;
	@Autowired
    private IFlowableTaskService iFlowableTaskService;
	
	@Override
	public List<Absent> listData(List<Absent> absentLists) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
    	List<Absent> absentsWarp = new ArrayList<Absent>();
    	for(Absent absent: absentLists) {
    		 String titleName = absent.getTitleName();
    		 String date = formatter.format(absent.getOperTime());
    		 absent.setTitleName(titleName+" "+date+" 的 请假单");
    		 
    		 FlowableMisModelForm misModelForm = flowableOfficeMapper.selectByBusinessTableId(absent.getId());
    		 absent.setProcDefId(misModelForm.getProcessDefinitionId());
    		 absent.setProcInsId(misModelForm.getProcessInstanceId());
    		 
    		 if(flowTaskService.getHistoryProcIns(misModelForm.getProcessInstanceId()).getEndActivityId()!=null) {
    			 absent.setProcessIsFinished("10002");//已完结(该流程已走完所有流程的审批过程，该流程已关闭)
    		 } else {
				absent.setProcessIsFinished("10001");//审核中(该流程正在审核过程中)
    		 }
    		 
    		 absentsWarp.add(absent);
    	 }
		return absentsWarp;
	}


	@Override
	public Message<String> save(Absent absent,Flowable flowable) {
		ShiroInfo shiroInfo = getShiroInfo();
   	 	String loginCode =shiroInfo.getUserCode();
        // 初次-发起流程
        if (StringUtils.isBlank(flowable.getBusinessId())) {
        	String modelKey = "leave-2020";//模型key
        	ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionKey(modelKey).latestVersion().singleResult();
        	flowable.setFormId(absent.getId());
            FlowFormBusiness formBusiness = new FlowFormBusiness(flowable.getFormId());
            formBusiness.preInsert();
            String procIns="";
            Map<String, Object> variables1 = new LinkedHashMap<>();
            try {
            	FlowableStartProcessInstance spiv = new FlowableStartProcessInstance();
            	spiv.setProcessDefinitionKey(modelKey);
            	spiv.setBusinessKey(UUIDUtils.counter());
            	String leaveId = UUIDUtils.counter();
                FlowableStartProcessInstance flowableStartProcessInstance = new FlowableStartProcessInstance();
                flowableStartProcessInstance.setBusinessKey(leaveId);
                flowableStartProcessInstance.setCreator(loginCode);
                flowableStartProcessInstance.setCurrentUserCode(loginCode);
                flowableStartProcessInstance.setFormName("资产登记流程");
                flowableStartProcessInstance.setProcessDefinitionKey(modelKey);
                flowableStartProcessInstance.setProcessDefinition(processDefinition);
                variables1=initLeave2020Variables(absent.getDays());
                flowableStartProcessInstance.setVariables(variables1);
                Message<ProcessInstance> returnStart = iFlowableProcessInstanceService.startProcessInstanceByKey(flowableStartProcessInstance);
                if (returnStart.getCode() == 100){
                	procIns = returnStart.getData().getProcessInstanceId();
                }
            }catch(Exception e) {
            	return Message.error(500, " 流程key： "+modelKey+" 的流程以被 '挂起', 请联系管理人员恢复该工作流程! ");
            }
            
            // 保存流程实例关联模型版本信息
            formBusiness.setProcInsId(procIns);
            String relationUrl = flowableModelMapper.selectImsModelByModelKey(modelKey).getFormUrl();//url数据来源于模型关联表单
            String formUrl = relationUrl+"/"+absent.getId()+"?";
            Map<String,Object> map = JsonTransferUtils.keyReversalValueMap(variables1);
            if(flowableOfficeMapper.insertImsFlowRelationTable(UUIDUtils.counter(),absent.getId(),processDefinition.getId(),procIns,modelKey,formUrl,new Date(),JsonTransferUtils.mapConvertToJson(variables1),JsonTransferUtils.mapConvertToJson(map))>0) {
            	System.out.println("流程实例ID： "+procIns);
            	return Message.success("添加流程表单成功啦!");
            }else {
            	return Message.success("流程启动失败!");
            }
        }
        return Message.success("流程启动成功啦!");
        //提交流程-end
	}
	
	/**
	 * @name 初始化流程参数
	 * @author:HuiJia
	 * @param days 天数
	 * @return map集合
	 */
	public Map<String, Object> initLeave2020Variables(Integer days){

		//获取参数,设置参数对象
		Map<String, Object> variables = new LinkedHashMap<String, Object>();
		
		//相关参数-------------->张三
		variables.put("zhangsan", "003");
		
		//相关参数-------------->李四
		variables.put("lisi", "004");
				
		//相关参数-------------->王五、赵六(设置二个人作为多实例的人员)		
		List<String> instances = new ArrayList<String>();
		instances.add("005");
		instances.add("006");
		variables.put("instance", instances);
		variables.put("num",2);
		
		//相关参数-------------->陈七
		variables.put("chenqi", "007");
		
		//相关参数-------------->谢八
		variables.put("xieba", "008");
		
		//相关参数-------------->徐九
		variables.put("xujiu", "009");
		
		//相关参数-------------->请假天数<=1 走张三审批，请假天数>1走李四审批
		variables.put("days", days);
		System.out.println("variables-------------------->" + variables);
		return variables;
	}


	@Override
	public Absent edit(Absent leave) {
		// 获取流程实例对象
		if (leave.getProcInsId() != null && !leave.getStatus().equals("lookup")) {
			ProcessInstance procIns = actTaskService.getProcIns(leave.getProcInsId());// 非发起流程
			if(procIns!=null) {
				leave.setBusinessId(procIns.getBusinessKey());
				System.out.println("编辑时的-BusinessId: "+leave.getBusinessId());
			}else {
				HistoricProcessInstance history = actTaskService.getHistoryProcIns(leave.getProcInsId());
				leave.setBusinessId(history.getBusinessKey());
				System.out.println("编辑时的-BusinessId: "+leave.getBusinessId());
			}
			System.out.println("编辑时的-TaskId: "+leave.getTaskId());
			System.out.println("编辑时的-ProcInsId: "+leave.getProcInsId());
			leave.setTaskId(leave.getTaskId());
			leave.setProcInsId(leave.getProcInsId());
			leave.setModelKey(leave.getModelKey());
			leave.setHasUnAuditPassFlow(false);
		}
		return leave;
	}


	@Override
	public void edit(Absent absent,Integer isAgree) {
		//审批
		if(absent.getIsAgree() != null) {
			if(absent.getIsAgree() == 1) { 
	        	FlowableCompleteTask flowableCompleteTask = new FlowableCompleteTask();
	        	flowableCompleteTask.setTaskId(absent.getTaskId());
	        	//completeTaskVo.setUserCode("test-5");
	        	flowableCompleteTask.setUserCode(getShiroInfo().getUserCode());//当前登录人
	        	flowableCompleteTask.setMessage(absent.getLeaderOpinion());
	        	flowableCompleteTask.setProcessInstanceId(absent.getProcInsId());
	        	flowableCompleteTask.setType("SP");
	        	iFlowableTaskService.complete(flowableCompleteTask);
			}
		}
	}

}
