/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.demo.entity;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.javamis.modular.flowable.entity.Flowable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @name :固定资产实体类；
 * 		<p>
 * 		    注：因使用了lombok，所以User实体类下的各属性不需要再创建get和set方法了,lombok内置已经包含get和set方法了
 *      </p>
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ass_storage")
public class AssStorage extends Flowable{
	//基础属性
	//@TableId(value="id", type= IdType.AUTO)//主键id
	private String id;					// 资产ID
	@TableField(value = "ass_code")
	private String assCode;				// 资产编码
	@TableField(value = "ass_budget")
	private String assBudget;			// 资产性质
	@TableField(value = "ass_brand")
	private String assBrand;			// 品牌
	@TableField(value = "ass_device_name")
	private String assDeviceName;		// 资产名称
	@TableField(value = "ass_spe_type")
	private String assSpeType;			// 资产型号
	@TableField(value = "ass_type")
	private String assType;				// 资产分类
	@TableField(value = "ass_start_date")
	private Date assStartDate;			// 开始使用日期/领用日期
	@TableField(value = "ass_pay_time")
	private Date assPayTime;			// 购置日期
	@TableField(value = "ass_place")
	private String assPlace;			// 存放地点
	@TableField(value = "ass_count")
	private Integer assCount=1;			// 资产数量
	@TableField(value = "pms_supplier_name")
	private String pmsSupplierName;		// 供应商
	@TableField(value = "ass_location")
	private String assLocation;			// 详细地点
	@TableField(value = "ass_use_life")
	private String assUseLife;			// 使用年限
	@TableField(value = "ass_manufacturer")
	private String assManufacturer;		// 生产厂商
	@TableField(value = "ass_war_expiration_date")
	private Date assWarExpirationDate;	// 保修截止日期
	@TableField(value = "ass_contract_money")
	private Double assContractMoney;	// 资产原值
	@TableField(value = "ass_original_code")
	private String assOriginalCode;		// 原资产编码
	@TableField(value = "ass_serial")
	private String assSerial;			// 序列号
	@TableField(value = "ass_device_origin_type")
	private String assDeviceOriginType;	// 来源方式
	@TableField(value = "ass_batch")
	private String assBatch;			// 批次号
	@TableField(value = "pms_accept_number")
	private String pmsAcceptNumber;		// 验收单号
	@TableField(value = "ass_entering_date")
	private Date assEnteringDate;		// 录入日期/入库日期
	@TableField(value = "ass_entry")
	private String assEntry;			// 录入人
	@TableField(value = "ass_snid")
	private String assSnid;				// SN号-序列号
	@TableField(value = "ass_util")
	private String assUtil;				//计量单位

	//管理信息
	@TableField(value = "ass_affiliate_eduit_code")
	private String assAffiliateEduitCode;// 所属单位编码
	@TableField(value = "ass_affiliate_eduit")
	private String assAffiliateEduit;	// 所属单位
	@TableField(value = "ass_use_type")
	private String assUseType;			// 使用类型
	@TableField(value = "ass_custodians_code")
	private String assCustodiansCode;	// 保管人编码(使用人/领用人)
	@TableField(value = "ass_custodians_name")
	private String assCustodiansName;	// 保管人名字(使用人/领用人)
	@TableField(value = "ass_account")
	private String assAccount;			// 资产账套
	@TableField(value = "ass_invoice_date")
	private Date assInvoiceDate;		//发票日期
	@TableField(value = "ass_invoice_no")
	private String assInvoiceNo;		//发票号
	@TableField(value = "ass_picture")
	private String assPicture;			//照片
	@TableField(value = "ass_accessory")
	private String assAccessory;		//附件
	@TableField(value = "ass_used")
	private String assUsed;				//资产用途
	@TableField(value = "scrap_id")
	private Long scrapId;				//报废表单号
	@TableField(value = "ass_scrap_date")
	private Date assScrapDate;			//报废日期
	@TableField(value = "ems_recorded_date")
	private Date emsRecordedDate;		//入账日期
	@TableField(value = "ems_mark_id")
	private String emsMarkId;			//入账编号
	@TableField(value = "ass_budget_department_code")
	private String assBudgetDepartmentCode;//预算部门
	@TableField(value = "ass_budget_department")
	private String assBudgetDepartment;	//预算部门
	@TableField(value = "ass_jiuqi_code")
	private String assJiuqiCode;		//久其编码
	@TableField(value = "ass_use_category")
	private String assUseCategory;		//使用类别
	@TableField(value = "ass_product_line_code")
	private String assProductLineCode;	//产品线编码
	@TableField(value = "ass_product_line")
	private String assProductLine;		//产品线
	@TableField(value = "ass_project_name")
	private String assProjectName;		//项目名称
	@TableField(value = "ass_source")
	private String assSource;			// 资金来源
	@TableField(value = "ass_remarks")
	private String assRemarks;			//备注
	@TableField(value = "maintain_id")
	private Long maintainId;			//维修单号
	@TableField(value = "cat_typeId")
	private Long catTypeid;				//分类编号
	@TableField(value = "allot_id")
	private String allotId;				//调拨单号
	@TableField(value = "alteration_number")
	private Long alterationNumber;		//变更编号
	@TableField(value = "user")
	private String user;				//当前办理人
	@TableField(value = "creator")
	private String creator;				//创建人
	@TableField(value = "creator_id")
	private String creatorId;   		//创建人Id
	@TableField(value = "extend_d1")
	private Date extendD1;				//创建时间
	@TableField(value = "dep_code")
	private String depCode;				//副资产编码
	@TableField(value = "creator_dept_id")
	private String creatorDeptId;
	@TableField(value = "ass_category")
	private String assCategory;			//资产分类-6d大分类
	@TableField(value = "brand_code")
	private String brandCode;
	@TableField(value = "pms_pur_depart_code")
	private String pmsPurDepartCode;	//申购部门(使用部门)编码
	@TableField(value = "pms_pur_depart_name")
	private String pmsPurDepartName;	//申请部门(使用部门)名称
	@TableField(value = "ass_save_type")
	private String assSaveType="";		//保存类型(1:保存 2:修改)
	@TableField(value = "ass_situation_code")
	private String assSituationCode;	//资产状态编码
	@TableField(value = "ass_situation_name")
	private String assSituationName;	//资产状态名称
	@TableField(value = "is_pms")
	private String isPms;				//是否来自采购系统
	@TableField(value = "process_instance_id")
	private String processInstanceId;	//流程实例ID
	//同意驳回(同意:1;驳回:2)
	@TableField(exist = false)
	private Integer isAgreement;
	@TableField(value = "amount")
	private Integer amount;				//申请金额
	/**
	 * @description 设置检索条件数据
	 * @param dict
	 * @return
	 */
	public Map<String, Object> setData(AssStorage assStorage){
		Map<String, Object> queryMap = new HashMap<>();
		if(assStorage.getAssCode() !="" && assStorage.getAssCode() !=null) {
	 		queryMap.put("ass_code", assStorage.getAssCode());
	 	}if(assStorage.getAssDeviceName() !=""&& assStorage.getAssDeviceName() !=null) {
	 		queryMap.put("ass_device_name", assStorage.getAssDeviceName());
	 	}if(assStorage.getAssType() !=""&& assStorage.getAssType() !=null) {
	 		queryMap.put("ass_type", assStorage.getAssType());
	 	}if(assStorage.getAssCustodiansName() !=""&& assStorage.getAssCustodiansName() !=null) {
	 		queryMap.put("ass_custodians_name", assStorage.getAssCustodiansName());
	 	}
	 	return queryMap;
	}
}
