/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.demo.controller;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.validation.Valid;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javamis.common.logger.Logger;
import com.javamis.common.logger.LoggerModule;
import com.javamis.common.logger.LoggerType;
import com.javamis.common.message.Message;
import com.javamis.common.page.PageWarpper;
import com.javamis.common.util.UUIDUtils;
import com.javamis.modular.demo.dao.AssStorageMapper;
import com.javamis.modular.demo.entity.AssStorage;
import com.javamis.modular.demo.service.AssStorageService;
import com.javamis.modular.flowable.entity.Flowable;
import javax.servlet.http.HttpServletRequest;

/**
 * @name:   资产管理Controller层
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Controller
@RequestMapping("/assStorage")
@SuppressWarnings("rawtypes")
public class AssStorageController {
    @Autowired
    private AssStorageMapper assStorageMapper;
    @Autowired
    private AssStorageService assStorageService;
    
    /**
     * 资产管理查询列表
     */
    //@RequiresPermissions("user:list")
    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String list(Model model) {
    	return "demo/storage/list";
    }
    
    
    @RequestMapping(value = "listData")
    @ResponseBody
    @SuppressWarnings("unlikely-arg-type")
    public PageWarpper<AssStorage> listData(AssStorage assStorage){
    	PageWarpper<AssStorage> pageWarpper = new PageWarpper<AssStorage>();
    	IPage<AssStorage> page = new Page<>(pageWarpper.getCurrentPage(), pageWarpper.getPageSize());
    	QueryWrapper<AssStorage> queryWrapper = new QueryWrapper<>();
    	Map<String, Object> queryMap = assStorage.setData(assStorage);
	 	String sort = pageWarpper.getSortName();
	 	String sortOrder =  pageWarpper.getSortOrder();
    	if(queryMap.isEmpty() && sort == null) {
    		assStorageMapper.selectPage(page, null);
    	}if(queryMap.isEmpty() && sort != null) {
    		if(sortOrder.equals("desc")) {
    			queryWrapper.orderByDesc(sort);
    		}if(sortOrder.equals("asc")) {
    			queryWrapper.orderByAsc(sort);
    		}
    		assStorageMapper.selectPage(page, queryWrapper);
    	 }if(!queryMap.isEmpty()) {
    		 queryWrapper.allEq(queryMap);
    		 assStorageMapper.selectPage(page, queryWrapper);
    	 }
    	 pageWarpper.setTotal(new Long(page.getTotal()).intValue());
    	 List<AssStorage> assStorages = page.getRecords();
    	 List<AssStorage> assStoragesWarp = assStorageService.listData(assStorages);
    	 //pageWarpper.setRows(assStorages);
    	 pageWarpper.setRows(assStoragesWarp);
    	 return  pageWarpper;
    }
    

    /**
     * 资产管理表单页面
     * @param model
     * @return
     */
    //@RequiresPermissions("user:show")
    @RequestMapping(value = "form")
    public String form() {
    	return "demo/storage/form";
    }
    
    /**
     * 添加流程表单
     */
    @RequestMapping("/save")
    @ResponseBody
    //@Logger(detail = "通过手机号[{{tel}}]获取资产管理名",level = 3,unit = LoggerModule.USER,type = LoggerType.INSERT)
    public Message<String> save(@Valid AssStorage assStorage,Flowable flowable, HttpServletRequest request) {
    	assStorage.setId(UUIDUtils.counter());
        assStorageMapper.insert(assStorage);
        return assStorageService.save(assStorage, flowable);
    }
    
    /**
     * 根据资产管理id，修改资产管理信息
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value="edit/{id}")
    public String edit(@PathVariable String id, Flowable flowable,Model model) {
    	AssStorage assStorage = assStorageMapper.selectById(id);
    	model.addAttribute("storage",assStorage);
    	model.addAttribute("flowable",assStorageService.edit(flowable));
    	return "demo/storage/edit";
    }
    
    /**
     * 编辑资产
     */
    @RequestMapping("/edit")
    @ResponseBody
    public Message<String> edit(@Valid AssStorage assStorage,Flowable flowable, BindingResult result) {
        this.assStorageMapper.updateById(assStorage);
        //如果业务id不为空,那么进行流程审批“同意”或“驳回”
        if (StringUtils.isNotBlank(flowable.getBusinessId())) {
        	System.out.println("编辑时的BusinessId: "+flowable.getBusinessId());
        	assStorageService.edit(assStorage, flowable);
        }
        return Message.success("办理事项成功啦!");
    }

    /**
     * 删除流程模型
     */
    @RequestMapping(value="/delete")
    @ResponseBody
    @Logger(unit = LoggerModule.ASSSTORAGE,type = LoggerType.DELETE)
    public Message delete(@RequestParam String id) {
        if(this.assStorageMapper.deleteById(id) != 1) {
        	return Message.error(500,"删除失败");
        }
        return Message.success();
    }
}
