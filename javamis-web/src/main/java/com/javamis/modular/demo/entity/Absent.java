/**
 * Copyright (c) 2016-Now http://www.javamis.com All rights reserved.
 * No deletion without permission, or be held responsible to law.
 */
package com.javamis.modular.demo.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.javamis.modular.flowable.entity.Flowable;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * @name :请假实体类；
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_leave")
public class Absent extends Flowable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
	//@TableId(value="id", type= IdType.AUTO)
	private String id;
    /**
     * 请假类型(公休、病假、事假、调休、婚假)
     */
	private String type;
    /**
     * 请假天数
     */
	@TableField(value = "days")
	private Integer days;
	/**
     * 请假原因
     */
	private String reason;
    /**
     * 领导意见
     */
	@TableField(value = "leaderOpinion")
	private String leaderOpinion;
	 
	/**
	 * 请假申请时间
	 */
	@TableField(value = "operTime")
	private Date operTime;
	
	/**
	 * 请假标题名称
	 */
	@TableField(value = "titleName")
	private String titleName;
    /**
     * 状态(1：启用  2：冻结  3：删除）
     */
	//private Integer status;

	public Absent() {}
	
	/**
	 * @description 设置检索条件数据
	 * @param leave 请假类
	 * @return
	 */
	public Map<String, Object> setData(Absent leave){
		Map<String, Object> queryMap = new HashMap<>();
		if(!"0".equals(leave.getType())&&!"".equals(leave.getType()) && leave.getType() !=null) {
	 		queryMap.put("type", leave.getType());
	 	}if(leave.getDays() !=null&&leave.getDays()!=0) {
	 		queryMap.put("days", leave.getDays());
	 	}if(leave.getReason() !="" && leave.getReason() !=null) {
	 		queryMap.put("reason", leave.getReason());
	 	}if(leave.getLeaderOpinion() !="" && leave.getLeaderOpinion() !=null) {
	 		queryMap.put("leaderOpinion", leave.getLeaderOpinion());
	 	}if(leave.getStatus() !=null) {
	 		queryMap.put("status", leave.getStatus());
	 	}
	 	return queryMap;
	}
	
}
