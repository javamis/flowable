package com.javamis.modular.demo.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.javamis.common.message.Message;
import com.javamis.modular.demo.entity.Absent;
import com.javamis.modular.flowable.entity.Flowable;

/**
 * @name:   请假单类实现接口
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
public interface AbsentService extends IService<Absent>{
	
	/**
	 * @name 重新封装标题内容；将流程定义、流程实例封装到分页列表数据集中
	 * @param params 封装后的分页列表数据
	 * @return 封装后的分页列表数据集
	 */
	public List<Absent> listData(List<Absent> absentLists);
	
	/**
	 * @name 启动流程
	 * @param absent 请假单实体
	 * @param flowable    流程实体
	 */
	public Message<String> save(Absent absent,Flowable flowable);
	
	/**
	 * @name 封装必要的参数
	 * @param absent 请假单
	 * @return 流程实例
	 */
	public Absent edit(Absent absent);
	
	/**
	 * @name 审批流程
	 * @param isAgree 审批时，是否同意
	 */
	public void edit(Absent absent,Integer isAgree);
}
