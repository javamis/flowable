package com.javamis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import com.javamis.modular.flowable.service.FlowableUniqueNameGenerator;

/**
 * @name:   JavaMis启动类
 * @version:1.0.2
 * @author:	HuiJia
 * @QQ号： 	763236277
 * @QQ群： 	327773608
 * @邮箱: 	javamis@163.com
 * @site：	http://www.javamis.com
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@MapperScan({"com.javamis.modular.*.dao","com.javamis.modular.*.dao.mapping","com.javamis.modular.*.entity","com.javamis.modular.flowable.dao","com.javamis.modular.flowable.dao.mapping","com.javamis.modular.flowable.entity"})		//,"com.javamis.modular.business.preregistration.service"解决business下的dao和service无法被扫描到的问题
@ComponentScan(nameGenerator = FlowableUniqueNameGenerator.class)
public class AdminApplication extends SpringBootServletInitializer{

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
    //为了打war包-4, 启动类继承SpringBootServletInitializer实现configure
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {return builder.sources(AdminApplication.class);}
}
